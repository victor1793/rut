from app import app
import os

port_env = os.environ.get("APP_PORT")
if not port_env:
    port_env = 8080
else:
    port_env = int(port_env)

debug_env = os.environ.get("APP_DEBUG")
if not debug_env:
    debug_env = True
else:
    debug_env = bool(debug_env)

app.run(host='127.0.0.1', port=5000, debug=True)
