from app import db

class Choferes(db.Model):
    __tablename__               = 'choferes'
    id                          = db.Column(db.Integer, primary_key=True)
    nombres                     = db.Column(db.String(100))
    apellidos                   = db.Column(db.String(100))
    email                       = db.Column(db.String(100))
    password                    = db.Column(db.String(100))
    firebase_token              = db.Column(db.String(150))
    firebase_token_notification = db.Column(db.String(150))
    token_conekta               = db.Column(db.String(150))
    sesion_unica                = db.Column(db.Integer)
    imagen                      = db.Column(db.String(100))
    ine                         = db.Column(db.String(100))
    comprobante_domicilio       = db.Column(db.String(100))
    telefono                    = db.Column(db.Integer)
    fecha_nacimiento            = db.Column(db.Date)
    fecha_actualizacion         = db.Column(db.DateTime)
    fecha                       = db.Column(db.DateTime)
    status                      = db.Column(db.Integer)

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def dump(self):
        return dict([('id', self.id), ('nombres ', self.nombres ), ('apellidos', self.apellidos),
                 ('email', self.email), ('password', self.password), ('status', self.status),
                 ('firebase_token', self.firebase_token), ('sesion_unica', self.sesion_unica),('imagen', self.imagen),
                 ('ine', self.ine),('telefono', self.telefono),('fecha_nacimiento', self.fecha_nacimiento),
                 ('firebase_token_notification', self.firebase_token_notification),('comprobante_domicilio', self.comprobante_domicilio),
                  ('token_conekta',self.token_conekta)])