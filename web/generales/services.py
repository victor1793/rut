from app import db, logger, app
from app.srv_choferes.model import Choferes
from datetime import datetime
from firebase_admin import auth



def addUserFromWeb(args):

    if( len(args['password']) < 6 ):
        return 0;

    driver = auth.create_user(
        email          = args['email'],
        email_verified = False,
        password       = args['password'],
        display_name   = args['nombre'],
        photo_url      ='https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.stickpng.com%2Fassets%2Fimages%2F585e4bcdcb11b227491c3396.png&f=1',
        disabled       = False
    )

    print('Sucessfully created new user: {0}'.format(driver.uid))

    aux = args['fecha_nacimiento'].split('/')
    data_split = str(aux[2]) + '-' + str(aux[0]) + '-' + str(aux[1])


    chofer = Choferes()
    chofer.nombres                     = args['nombre']
    chofer.email                       = args['email']
    chofer.password                    = args['password']
    chofer.firebase_token              = driver.uid
    chofer.fecha                       = datetime.now()
    chofer.status                      = 1
    chofer.sesion_unica                = 0
    chofer.ine                         = ''
    chofer.imagen                      = ''
    chofer.apellidos                   = ''
    chofer.comprobante_domicilio       = ''
    chofer.firebase_token_notification = ''
    chofer.telefono                    = args['telefono']
    chofer.token_conekta               = ''
    chofer.fecha_nacimiento            = data_split



    db.session.add(chofer)
    db.session.commit()


    return chofer.id
