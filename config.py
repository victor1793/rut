'''Define the application directory'''
import os
from app import logger
import firebase_admin
from firebase_admin import credentials, auth
'''Por definir'''
PRODUCTION_PATH = ''

'''Servidor local de pruebas'''
DEVELOPMENT_PATH_LINUX = ''
DEVELOPMENT_PATH_WINDOWS = r'C:\Users\Touches\Desktop\rut'
DEVELOPMENT_PATH_IRAS = '/home/touches/Documentos/Servicios/rut'

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
print("\n\nBASEDIR: "+str(BASE_DIR))
DEBUG = os.environ.get("FLASK_DEBUG", True)

'''config secret key'''
SECRET_KEY = 'iras1793'

cred = credentials.Certificate(r'/home/touches/Documentos/Servicios/rut/client_secret_577292463756_5lq5a.json')
default_app = firebase_admin.initialize_app(cred)

'''Entorno de desarrollo para linux'''
if BASE_DIR == DEVELOPMENT_PATH_IRAS:
    logger.info('\n\n ENTRAMOS AL ENTORNO DE LINUX:')

    DB_URI = 'mysql+pymysql://root@127.0.0.1/touches_rut'

    SQLALCHEMY_DATABASE_URI = DB_URI
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    CHOFERES_PROFILE_PATH             = os.environ.get("APP_PROFILE_IMG_PATH",DEVELOPMENT_PATH_LINUX + '/media/choferes/perfil/')
    CHOFERES_CARTAS_ANTECEDENTES_PATH = os.environ.get("APP_ANTECEDENTES_PATH",DEVELOPMENT_PATH_LINUX + '/media/choferes/cartaAntecedentesPenales/')
    CHOFERES_INE_PATH                 = os.environ.get("APP_INE_PATH", DEVELOPMENT_PATH_LINUX + '/media/choferes/ine/')
    CHOFERES_COMPROBANTE_DOMICILIO    = os.environ.get("APP_COMPROBANTE_PATH", DEVELOPMENT_PATH_LINUX + '/media/choferes/comprobantes_domicilio/')
    CHOFERES_AUTO_PATH                = os.environ.get("APP_AUTO_PATH", DEVELOPMENT_PATH_LINUX + '/media/choferes/autos/')
    CHOFERES_LICENCIA_PATH            = os.environ.get("APP_LICENCIA_PATH", DEVELOPMENT_PATH_LINUX + '/media/choferes/licencia/')
    CHOFERES_TARJETA_PATH             = os.environ.get("APP_TARJETA_PATH", DEVELOPMENT_PATH_LINUX + '/media/choferes/tarjetaCirculacion/')
    CHOFERES_POLIZA_PATH              = os.environ.get("APP_POLIZA_PATH", DEVELOPMENT_PATH_LINUX + '/media/choferes/poliza/')
    CHOFERES_VERIFICACION_PATH        = os.environ.get("APP_VERIFICACION_PATH", DEVELOPMENT_PATH_LINUX + '/media/choferes/verificacion/')



    USERS_PROFILE_PATH          = os.environ.get("APP_USER_PROFILE_PATH", DEVELOPMENT_PATH_LINUX + '/media/usuarios/perfil/')
    USERS_INE_PATH              = os.environ.get("APP_INE_USER_PATH", DEVELOPMENT_PATH_LINUX + '/media/usuarios/ine/')
    USERS_COMPROBANTE_DOMICILIO = os.environ.get("APP_COMPROBANTE_USER_PATH", DEVELOPMENT_PATH_LINUX + '/media/usuarios/comprobantes_domicilio/')


elif BASE_DIR == DEVELOPMENT_PATH_WINDOWS:

    userpass = 'mysql+pymysql://root:@'
    basedir = '127.0.0.1'
    dbname = '/touches_rut'

    SQLALCHEMY_DATABASE_URI = userpass + basedir + dbname
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    CHOFERES_PROFILE_PATH = os.environ.get("APP_PROFILE_IMG_PATH",DEVELOPMENT_PATH_WINDOWS + '/media/choferes/perfil/')
    CHOFERES_CARTAS_ANTECEDENTES_PATH = os.environ.get("APP_ANTECEDENTES_PATH",DEVELOPMENT_PATH_WINDOWS + '/media/choferes/cartaAntecedentesPenales/')
    CHOFERES_INE_PATH = os.environ.get("APP_INE_PATH", DEVELOPMENT_PATH_WINDOWS + '/media/choferes/ine/')
    CHOFERES_COMPROBANTE_DOMICILIO = os.environ.get("APP_COMPROBANTE_PATH", DEVELOPMENT_PATH_WINDOWS + '/media/choferes/comprobantes_domicilio/')

    USERS_PROFILE_PATH = os.environ.get("APP_USER_PROFILE_PATH", DEVELOPMENT_PATH_WINDOWS + '/media/usuarios/perfil/')
    USERS_INE_PATH = os.environ.get("APP_INE_USER_PATH", DEVELOPMENT_PATH_WINDOWS + '/media/usuarios/ine/')
    USERS_COMPROBANTE_DOMICILIO = os.environ.get("APP_COMPROBANTE_USER_PATH", DEVELOPMENT_PATH_WINDOWS + '/media/usuarios/comprobantes_domicilio/')

    FIREBASE_ADMIN_JSON = os.environ.get("FIREBASE_ADMIN", DEVELOPMENT_PATH_WINDOWS +'/client_secret_577292463756_5lq5a.json')
else:

    print("Error")

SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get("DB_TRACK_MODIFICATIONS", False)
DATABASE_CONNECT_OPTIONS = {}
SQLALCHEMY_ECHO = os.environ.get("DB_ECHO", True)

# Mail server settings
MAIL_SERVER = os.environ.get("MAIL_SERVER", 'smtp.gmail.com')
MAIL_PORT = os.environ.get("MAIL_PORT", 465)
MAIL_USERNAME = os.environ.get("MAIL_PORT", 'pruebasRUT@gmail.com')
MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD", 'rut-12345')
MAIL_USE_TLS = os.environ.get("MAIL_USE_TLS", False)
MAIL_USE_SSL = os.environ.get("MAIL_USE_SSL", True)

# administrador lista de correo(s)
ADMINS = os.environ.get("MAIL_ADMINS", ['pruebasRUT@gmail.com'])

# App configurations
# CLIMA_URL = os.environ.get("APP_CLIMA_URL", 'https://api.darksky.net/forecast/fbf7df90004c43e439d48d902445606b/')

# Recover password
# RECOVERY_MSG_BODY = 'Te enviamos el enlace para recuperar tu password. El link expirara en 48 horas. https://movil.luxelare.com/recovery/users/email/request/2/%s'
