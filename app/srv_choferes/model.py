from app import db

class Choferes(db.Model):
    __tablename__ = 'choferes'
    id                          = db.Column(db.Integer, primary_key=True)
    nombres                     = db.Column(db.String(100))
    apellidos                   = db.Column(db.String(100))
    email                       = db.Column(db.String(100))
    password                    = db.Column(db.String(100))
    firebase_token              = db.Column(db.String(150))
    firebase_token_notification = db.Column(db.String(150))
    token_conekta               = db.Column(db.String(150))
    sesion_unica                = db.Column(db.Integer)
    id_auto                     = db.Column(db.Integer)
    imagen                      = db.Column(db.String(100))
    ine                         = db.Column(db.String(100))
    comprobante_domicilio       = db.Column(db.String(100))
    antecedentes                = db.Column(db.String(100))
    telefono                    = db.Column(db.Integer)
    pais                        = db.Column(db.String(100))
    estado                      = db.Column(db.String(100))
    fecha_nacimiento            = db.Column(db.Date)
    fecha_actualizacion         = db.Column(db.DateTime)
    fecha                       = db.Column(db.DateTime)
    status                      = db.Column(db.Integer)
    ine_status                  = db.Column(db.Integer)
    comprobante_status          = db.Column(db.Integer)
    antecedentes_status         = db.Column(db.Integer)
    id_auto                     = db.Column(db.Integer)
    asignado                    = db.Column(db.Integer)
    rating                      = db.Column(db.Float)

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def dump(self):
        return dict([('id', self.id), ('nombres ', self.nombres ), ('apellidos', self.apellidos),
                 ('email', self.email), ('password', self.password), ('status', self.status),
                 ('firebase_token', self.firebase_token), ('sesion_unica', self.sesion_unica),('imagen', self.imagen),
                 ('ine', self.ine),('telefono', self.telefono),('fecha_nacimiento', self.fecha_nacimiento),
                 ('firebase_token_notification', self.firebase_token_notification),('comprobante_domicilio', self.comprobante_domicilio),
                 ('token_conekta',self.token_conekta),('ine_status',self.ine_status),('comprobante_status',self.comprobante_status)
                 ('antecedentes_status', self.antecedentes_status),('rating', self.rating),('id_chofer', self.id_chofer),('asignado', self.asignado)])


class UbicacionChoferes(db.Model):
    __tablename__ = 'ubicacionChoferes'
    id       = db.Column(db.Integer, primary_key=True)
    latitud  = db.Column(db.String(100))
    longitud = db.Column(db.String(100))
    idchofer = db.Column(db.Integer)
    fecha    = db.Column(db.DateTime)
    status   = db.Column(db.Integer)

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def dump(self):
        return dict([('id',self.id), ('latitud',self.latitud), ('longitud',self.longitud), ('idchofer',self.idchofer)])

class DatosBancarios(db.Model):
    __tablename__ = 'datos_bancarios'
    id         = db.Column(db.Integer, primary_key=True)
    id_chofer  = db.Column(db.Integer)
    clabe      = db.Column(db.String(100))
    banco      = db.Column(db.String(100))
    cuenta     = db.Column(db.String(100))
    fecha      = db.Column(db.DateTime)
    fecha_baja = db.Column(db.DateTime)
    status     = db.Column(db.Integer)

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def dump(self):
        return dict([ ('id',self.id), ('id_chofer',self.id_chofer), ('clabe',self.clabe), ('banco',self.banco),
            ('cuenta', self.cuenta),('fecha', self.fecha) ])

class Autos(db.Model):
    __tablename__ = 'autos'
    id           = db.Column(db.Integer, primary_key=True)
    id_chofer    = db.Column(db.Integer)
    placas       = db.Column(db.String(100))
    color        = db.Column(db.String(100))
    id_tipo_auto = db.Column(db.Integer)
    marca        = db.Column(db.String(100))
    puertas      = db.Column(db.String(100))
    descripcion  = db.Column(db.String(500))
    imagen       = db.Column(db.String(100))
    modelo       = db.Column(db.String(100))
    licencia     = db.Column(db.String(100))
    tarjeta      = db.Column(db.String(100))
    poliza       = db.Column(db.String(100))
    verificacion = db.Column(db.String(100))
    fecha        = db.Column(db.DateTime)
    fecha_baja   = db.Column(db.DateTime)
    status       = db.Column(db.Integer)

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def dump(self):
        return dict([ ('id',self.id), ('id_chofer',self.id_chofer), ('placas',self.placas), ('color',self.color),
            ('id_tipo_auto', self.id_tipo_auto),('marca', self.marca),('puertas', self.puertas),('imagen', self.imagen),
            ('descripcion', self.descripcion),('modelo', self.modelo),('licencia', self.licencia),
            ('tarjeta', self.tarjeta),('poliza', self.poliza),('verificacion', self.verificacion),('fecha', self.fecha) ])