from flask import request, session
from flask_restful import Resource, reqparse
from app.helpers.helper import create_success_response, computeMD5hash
from app.helpers.decorator import authenticate_request
from app.generales.service import login, logout, generaJsonChofer
from app import logger, app
from app.srv_choferes.service import getChoferByToken,updateSesionUnica, updateUbicacion,updateChoferByToken,inserChofer,updateFCM,insertFCM,insertDatosBancarios,updateDatosBancarios,deleteDatosBancarios,listaDatosBancarios,insertAutoInfo,getAutoInfo,updateAuto,deleteAuto,updateStatusAsignado,insertDocumentosAuto

from app.helpers.helper import create_error_response
from app.excepciones.exception import BusinessException

from app.generales.service import getUsuarioByToken, generaJsonUsuario, generarErrorDelSistema

class LoginRestService():
    def login(self,firebase_token):
        respuesta = False
        firebase_token = 'chofer_'+firebase_token
        sesion_iniciada = login(firebase_token)
        if sesion_iniciada is not None:
            respuesta = True
        return respuesta

    def logout(self,firebase_token):
        respuesta = False
        firebase_token = 'chofer_' + firebase_token
        sesion_terminada = logout(firebase_token)
        if sesion_terminada is True:
            respuesta = True
        return respuesta

class ChoferRestService(Resource):
    '''peticion de autenticacion'''
    @authenticate_request
    def get(self,firebase_token,tipo):
        chofer_json = ''
        try:
            chofer_data = getChoferByToken(computeMD5hash(firebase_token))
            if chofer_data.firebase_token is not None:
                servicio_login = LoginRestService()
                servicio_login.login(chofer_data.firebase_token)
                if servicio_login is not None:
                    updateSesionUnica(firebase_token,1)
                    data_chofer = generaJsonChofer(chofer_data)
                    chofer_json = {'Chofer':data_chofer}
                else:
                    raise BusinessException('Error en inicio de sesion para el chofer')
            else:
                raise BusinessException('El token de este chofer no existe en la base')
        except BusinessException as e:
            return create_error_response(e.message)

    @authenticate_request
    def put(self,firebase_token,tipo):
        '''Funcion para actualizar un chofer en base a su token'''
        parser = reqparse.RequestParser()
        parser.add_argument('nombres',          required=False, help='')
        parser.add_argument('apellidos',        required=False, help='')
        parser.add_argument('email',            required=False, help='')
        parser.add_argument('telefono',         required=False, help='')
        parser.add_argument('fecha_nacimiento', required=False, help='')
        parser.add_argument('password',         required=False, help='')

        info_chofer = parser.parse_args()
        try:
            chofer_json = ''
            chofer_id = updateChoferByToken(firebase_token,info_chofer)
            if chofer_id > 0:
                chofer_json = {'Chofer update':'success','chofer_id':chofer_id}
            else:
                chofer_json = {'Chofer update':'error','chofer_id':0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(chofer_json)

    @authenticate_request
    def post(self,firebase_token,tipo):
        '''Funcion para agregar un chofer'''
        parser = reqparse.RequestParser()
        parser.add_argument('nombres',          required=True, help='parametro nombres   es requerido')
        parser.add_argument('apellidos',        required=True, help='parametro apellidos es requerido')
        parser.add_argument('email',            required=True, help='parametro email     es requerido')
        parser.add_argument('telefono',         required=True, help='parametro telefono  es requerido')
        parser.add_argument('estado',           required=True, help='parametro estado    es requerido')
        parser.add_argument('pais',             required=True, help='parametro pais      es requerido')
        parser.add_argument('fecha_nacimiento', required=True, help='parametro fecha_nacimiento es requerido')
        parser.add_argument('password',         required=False,help='')

        info_chofer = parser.parse_args()
        try:
            servicio_login = LoginRestService()
            servicio_login.login(firebase_token)
            if servicio_login is not None:
                #insertamos la informacion del chofer
                chofer_id = inserChofer(firebase_token, info_chofer)
                if chofer_id > 0:
                    #actualizo el campo de la sesion unica en la base
                    updateSesionUnica(firebase_token, 1)
                    chofer_json = {'Chofer Add': 'success', 'chofer_id': chofer_id}
                else:
                    chofer_json = {'Chofer Add': 'error', 'chofer_id': 0}



        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(chofer_json)

class Ubicaciones(Resource):
    @authenticate_request
    def post(self,firebase_token,tipo):
        '''Funcion encargada de actualizar constantemente la ubicacion de cada chofer'''
        parser = reqparse.RequestParser()
        parser.add_argument('latitud',  required=True, help='parametro latitud  es requerido')
        parser.add_argument('longitud', required=True, help='parametro longitud es requerido')
        parser.add_argument('idchofer', required=True, help='parametro idchofer es requerido')

        info_ubicaciones = parser.parse_args()
        try:
            info = updateUbicacion(info_ubicaciones)
            if info > 0:
                ubicacion_json = {'updateUbicacion':'success','idubicacion':info}
            else:
                ubicacion_json = {'updateUbicacion': 'error', 'idubicacion': 0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(ubicacion_json)

class DatosChoferRestService(Resource):
    @authenticate_request
    def get(self,firebase_token,tipo):
        chofer_json = ''
        try:
            chofer_data = getUsuarioByToken(computeMD5hash(firebase_token),tipo)
            if chofer_data.firebase_token is not None:
                data_driver = generaJsonChofer(chofer_data)
                chofer_json = {'Chofer': data_driver}
            else:
                raise BusinessException('El token de este chofer no existe en la base')
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(chofer_json)

class ChoferFCMRestService(Resource):
    @authenticate_request
    def post(self,firebase_token,tipo):
        fcm_json = ''
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('fcm', required=True, help='')
            info_fcm = parser.parse_args()
            id_chofer = insertFCM(firebase_token,info_fcm)
            if id_chofer > 0:
                fcm_json = {'Chofer add fcm': 'success', 'id_chofer': id_chofer}
            else:
                fcm_json = {'Chofer add fcm': 'error', 'id_chofer': 0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(fcm_json)

    @authenticate_request
    def put(self,firebase_token,tipo):
        fcm_json = ''
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('fcm', required=True, help='')
            info_fcm = parser.parse_args()
            id_chofer = updateFCM(firebase_token,info_fcm)
            if id_chofer > 0:
                fcm_json = {'Chofer update fcm': 'success', 'usuario_id': id_chofer}
            else:
                fcm_json = {'Chofer update fcm': 'error', 'usuario_id': 0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(fcm_json)

class DatosBancariosRestService(Resource):

    @authenticate_request
    def get(self,firebase_token,tipo):
        datos_bancarios_json = ''
        try:
            lista = listaDatosBancarios(firebase_token)
            datos_bancarios_json = {'DatosBancarios':'success','lista':lista}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(datos_bancarios_json)



    @authenticate_request
    def post(self,firebase_token,tipo):
        datos_bancarios_json = ''
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('clabe',  required=True, help='parametro clabe  es requerido')
            parser.add_argument('banco',  required=True, help='parametro banco  es requerido')
            parser.add_argument('cuenta', required=True, help='parametro cuenta es requerido')

            info_datos = parser.parse_args()
            id_datos = insertDatosBancarios(firebase_token,info_datos)
            if id_datos > 0:
                datos_bancarios_json = {'DatosBancarios insert':'success','datos_bancarios_id':id_datos}
            else:
                datos_bancarios_json = {'DatosBancarios insert': 'error', 'datos_bancarios_id': 0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(datos_bancarios_json)

    @authenticate_request
    def put(self,firebase_token,tipo):
        datos_bancarios_json = ''
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('clabe',  required=False, help='')
            parser.add_argument('banco',  required=False, help='')
            parser.add_argument('cuenta', required=False, help='')
            parser.add_argument('id_datos_bancarios', required=True, help='parametro id_datos_bancarios es requerido')

            info_datos = parser.parse_args()
            id_datos = updateDatosBancarios(firebase_token,info_datos)
            if id_datos > 0:
                datos_bancarios_json = {'DatosBancarios update':'success','datos_bancarios_id':id_datos}
            else:
                datos_bancarios_json = {'DatosBancarios update': 'error', 'datos_bancarios_id': 0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(datos_bancarios_json)

    @authenticate_request
    def delete(self,firebase_token,tipo):
        datos_bancarios_json = ''
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('id_datos_bancarios', required=True, help='parametro id_datos_bancarios es requerido')

            info_datos = parser.parse_args()
            id_datos = deleteDatosBancarios(firebase_token,info_datos)
            if id_datos > 0:
                datos_bancarios_json = {'DatosBancarios delete':'success','datos_bancarios_id':id_datos}
            else:
                datos_bancarios_json = {'DatosBancarios delete': 'error', 'datos_bancarios_id': 0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(datos_bancarios_json)

class AutosRestService(Resource):

    @authenticate_request
    def get(self,firebase_token,tipo):
        auto_json = ''
        try:
            result    = 0
            respuesta = ''

            result,respuesta = getAutoInfo(firebase_token)
            if result < 0 :
                auto_json = {'AutoInfo': 'error', 'auto': []}
                generarErrorDelSistema('Error al obtener infomacion del auto con chofer: ' + str(firebase_token))
            else:
                auto_json = {'AutoInfo': 'success', 'auto': respuesta}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(auto_json)


    @authenticate_request
    def post(self,firebase_token,tipo):
        autos_json = ''
        try:
            id_auto   = 0
            id_chofer = 0
            respuesta = ''

            parser = reqparse.RequestParser()
            parser.add_argument('placas',    required=True, help='parametro placas    es requerido')
            parser.add_argument('color',     required=True, help='parametro color     es requerido')
            parser.add_argument('marca',     required=True, help='parametro marca     es requerido')
            parser.add_argument('puertas',   required=True, help='parametro puerta    es requerido')
            parser.add_argument('modelo',    required=True, help='parametro modelo    es requerido')

            datos_auto = parser.parse_args()
            id_auto, respuesta = insertAutoInfo(firebase_token,datos_auto)
            if id_auto > 0:
                updateStatusAsignado(firebase_token,id_auto)
                autos_json = {'Auto insert':'success','auto_id':id_auto}
            else:
                generarErrorDelSistema('Error al insertar un nuevo auto para el chofer: '+str(firebase_token))
                autos_json = {'Auto insert': 'error', 'auto_id': 0}
        except BusinessException as e:
            generarErrorDelSistema('Excepcion en clase Autos metodo post: '+str(e.message)+' firebase:'+str(firebase_token))
            return create_error_response(e.message)
        return create_success_response(autos_json)


    @authenticate_request
    def put(self,firebase_token,tipo):
        autos_json = ''
        try:
            id_auto   = 0
            respuesta = ''

            parser = reqparse.RequestParser()
            parser.add_argument('placas',  required=False, help='')
            parser.add_argument('color',   required=False, help='')
            parser.add_argument('marca',   required=False, help='')
            parser.add_argument('puertas', required=False, help='')
            parser.add_argument('modelo',  required=False, help='')
            parser.add_argument('id_auto', required=True, help='parametro id_auto es requerido')

            info_auto = parser.parse_args()
            id_auto,respuesta = updateAuto(firebase_token,info_auto)
            if id_auto > 0:
                autos_json = {'Auto update': 'success', 'auto_id': id_auto}
            else:
                generarErrorDelSistema(respuesta)
                autos_json = {'Auto update': 'error', 'auto_id': 0}
        except BusinessException as e:
            generarErrorDelSistema('Error en clase autos, metodo put con firebase id: '+str(firebase_token)+', Error: '+str(e.message))
            return create_error_response(e.message)
        return create_success_response(autos_json)


    @authenticate_request
    def delete(self,firebase_token,tipo):
        auto_json = ''
        try:
            id_auto   = 0
            respuesta = ''

            parser = reqparse.RequestParser()
            parser.add_argument('id_auto', required=True, help='parametro id_auto es requerido')

            info_auto = parser.parse_args()
            id_auto , respuesta = deleteAuto(firebase_token,info_auto)
            if id_auto > 0:
                auto_json = {'Auto delete': 'success', 'auto_id': id_auto}
            else:
                generarErrorDelSistema(respuesta)
                auto_json = {'Auto update': 'error', 'auto_id': 0}
        except BusinessException as e:
            generarErrorDelSistema('Error en clase autos, metodo delete con firebase id: ' + str(firebase_token) + ', Error: ' + str(e.message))
            return create_error_response(e.message)
        return create_success_response(auto_json)

    @authenticate_request
    def patch(self,firebase_token,tipo):
        auto_json = ''
        try:
            id_auto   = 0
            respuesta = ''

            parser = reqparse.RequestParser()
            parser.add_argument('id_auto', required=True, help='parametro id_auto es requerido')

            info_auto = parser.parse_args()
            id_auto , respuesta = insertDocumentosAuto(firebase_token,info_auto)
            if id_auto > 0:
                auto_json = {'Documentos auto': 'success', 'auto_id': id_auto}
            else:
                generarErrorDelSistema(respuesta)
                auto_json = {'Documentos auto': 'error', 'auto_id': 0}
        except BusinessException as e:
            generarErrorDelSistema('Error en clase autos, metodo patch con firebase id: ' + str(firebase_token) + ', Error: ' + str(e.message))
            return create_error_response(e.message)
        return create_success_response(auto_json)