from flask import session,request
from app import logger, db,app
from app.srv_choferes.model import Choferes, UbicacionChoferes, DatosBancarios, Autos
from datetime import datetime
from app.helpers.helper import computeMD5hash
from app.excepciones.exception import BusinessException
from app.generales.model import ErroresEnSistema
from app.generales.service import generarErrorDelSistema, uploadDocumentosAuto
from werkzeug.utils import secure_filename
import hashlib
from os.path import isfile, join
import os
from sqlalchemy import create_engine, text, and_,or_,asc,desc,distinct,between

#-----------------------------------------------------------------------------------------------------------------------
def getChoferByToken(token):
    '''Funcion que retorna un chofer dependiedo su token'''
    chofer = Choferes.query.filter_by(firebase_token=token,status=1).first()
    return chofer
#-----------------------------------------------------------------------------------------------------------------------
def updateSesionUnica(token,valor):
    chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(token)).first()
    if not chofer:
        raise BusinessException('Chofer no encontrado, no se pudo actualizar la sesion')
    else:
        chofer.sesion_unica = valor
        db.session.commit()
#-----------------------------------------------------------------------------------------------------------------------
def insertUbicacion(args):
    '''Funcion encargada de insertar una nueva ubicacion, en caso de que el conductor no se encuentre
    en la tabla'''
    nueva_ubicacion = UbicacionChoferes()

    nueva_ubicacion.latitud  = args['latitud']
    nueva_ubicacion.longitud = args['longitud']
    nueva_ubicacion.idchofer = args['idchofer']
    nueva_ubicacion.fecha    = datetime.now()
    nueva_ubicacion.status   = 1

    db.session.add(nueva_ubicacion)
    db.session.commit()

    return nueva_ubicacion.id
#-----------------------------------------------------------------------------------------------------------------------
def updateUbicacion(args):
    '''Funcion encargada de actualizar la ubicacion del chofer en base a su id (no token)'''

    '''primero se busca al chofer en la tabla de ubicaciones'''
    chofer = UbicacionChoferes.query.filter_by(idchofer=args['idchofer']).first()
    resultado = 0
    if chofer is None:
        resultado = insertUbicacion(args)
    else:
        '''Se actualizan las coordenadas'''
        chofer.latitud  = args['latitud']
        chofer.longitud = args['longitud']
        db.session.commit()
        resultado = chofer.id
    return resultado
#-----------------------------------------------------------------------------------------------------------------------
def updateChoferByToken(firebase_token,args):
    #-- Funcion encargada de actualizar los datos del chofer en base al token
    chofer = Choferes.query.filter_by(firebase_token = computeMD5hash(firebase_token),status=1).first()
    if chofer is None:
        raise BusinessException('Chofer no encontraado, no se actualizo la informacion')
    else:
        chofer.nombres          = args['nombres'] if args['nombres'] is not None else chofer.nombres
        chofer.apellidos        = args['apellidos'] if args['apellidos'] is not None else chofer.apellidos
        chofer.email            = args['email'] if args['email'] is not None else chofer.email
        chofer.password         = computeMD5hash(args['password']) if args['password'] is not None else chofer.password
        chofer.telefono         = args['telefono'] if args['telefono'] is not None else chofer.telefono
        chofer.fecha_nacimiento = args['fecha_nacimiento'] if args['fecha_nacimiento'] is not None else chofer.fecha_nacimiento

        db.session.commit()
        return chofer.id
#-----------------------------------------------------------------------------------------------------------------------
def inserChofer(firebase_token,values):

    chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(firebase_token),status=1).first()

    if not chofer :
        raise BusinessException('chofer no encontrado con el firebase_token {0}'.format(firebase_token))

    aux = values['fecha_nacimiento'].split('-')
    data_split = str(aux[2]) + '-' + str(aux[1]) + '-' + str(aux[0])

    chofer.nombres                     = values['nombres']
    chofer.apellidos                   = values['apellidos']
    chofer.email                       = values['email']
    chofer.telefono                    = values['telefono']
    chofer.fecha_nacimiento            = data_split
    chofer.pais                        = values['pais']
    chofer.estado                      = values['estado']
    chofer.token_conekta               = ''
    chofer.comprobante_domicilio       = ''
    chofer.imagen                      = ''
    chofer.ine                         = ''
    chofer.antecedentes                = ''
    chofer.ine_status                  = 0
    chofer.rating                      = 0
    chofer.comprobante_status          = 0
    chofer.antecedentes_status         = 0
    chofer.firebase_token_notification = ''
    chofer.password                    = values['password']
    chofer.id_auto                     = 0
    chofer.asignado                    = 0
    db.session().commit()
    return chofer.id
#-----------------------------------------------------------------------------------------------------------------------
def updateFCM(firebase_token,args):
    chofer = Choferes.query.filter_by(firebase_token = computeMD5hash(firebase_token),status=1).first()
    if chofer is None:
        raise BusinessException('Chofer no encontrado, se detuvo la actualizacion de fcm')
    else:
        chofer.firebase_token_notification = args['fcm']
        db.session.commit()
        return chofer.id
#-----------------------------------------------------------------------------------------------------------------------
def insertFCM(firebase_token,args):
    chofer = Choferes.query.filter_by(firebase_token = computeMD5hash(firebase_token),status=1).first()
    if chofer is None:
        raise BusinessException('Chofer no encontrado, se detuvo la insercion de fcm')
    else:
        chofer.firebase_token_notification = args['fcm']
        db.session.commit()
        return chofer.id
#-----------------------------------------------------------------------------------------------------------------------
def insertDatosBancarios(firebase_token,args):
    chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(firebase_token), status=1).first()
    if chofer is None:

        error = ErroresEnSistema()

        error.error  = 'Chofer no encontrado, se detuvo la insercion de datos bancarios'
        error.fecha  = datetime.now()
        error.status = 1
        db.session.add(error)
        db.session.commit()

        raise BusinessException('Chofer no encontrado, se detuvo la insercion de datos bancarios')
    else:

        datosBancarios = DatosBancarios()

        datosBancarios.id_chofer = chofer.id
        datosBancarios.clabe     = args['clabe']
        datosBancarios.banco     = args['banco']
        datosBancarios.cuenta    = args['cuenta']
        datosBancarios.fecha     = datetime.now()
        datosBancarios.status    = 1

        db.session.add(datosBancarios)
        db.session.commit()
        return datosBancarios.id
#-----------------------------------------------------------------------------------------------------------------------
def updateDatosBancarios(firebase_token,args):
    chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(firebase_token), status=1).first()
    if chofer is None:

        error = ErroresEnSistema()

        error.error  = 'Chofer no encontrado, se detuvo la actualizacion de datos bancarios'
        error.fecha  = datetime.now()
        error.status = 1
        db.session.add(error)
        db.session.commit()

        raise BusinessException('Chofer no encontrado, se detuvo la actualizacion de datos bancarios')
    else:
        datosBancarios = DatosBancarios.query.filter_by(id=args['id_datos_bancarios'],id_chofer=chofer.id,status=1).first()
        if datosBancarios is None:
            error = ErroresEnSistema()

            error.error = 'Se encontró al chofer, pero no tiene tarjetas bancarias, no se completó la actualización'
            error.fecha = datetime.now()
            error.status = 1
            db.session.add(error)
            db.session.commit()
            return 0
        else:
            datosBancarios.banco  = args['banco'] if args['banco'] is not None else datosBancarios.banco
            datosBancarios.clabe  = args['clabe'] if args['clabe'] is not None else datosBancarios.clabe
            datosBancarios.cuenta = args['cuenta'] if args['cuenta'] is not None else datosBancarios.cuenta
            db.session.commit()
            return datosBancarios.id
#-----------------------------------------------------------------------------------------------------------------------
def deleteDatosBancarios(firebase_token,args):
    chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(firebase_token), status=1).first()
    if chofer is None:

        error = ErroresEnSistema()

        error.error  = 'Chofer no encontrado, se detuvo la eliminacion de datos bancarios'
        error.fecha  = datetime.now()
        error.status = 1
        db.session.add(error)
        db.session.commit()

        raise BusinessException('Chofer no encontrado, se detuvo la eliminacion de datos bancarios')
    else:
        datosBancarios = DatosBancarios.query.filter_by(id=args['id_datos_bancarios'],id_chofer=chofer.id,status=1).first()
        if datosBancarios is None:
            error = ErroresEnSistema()

            error.error = 'Se encontró al chofer, pero no tiene datos bancarios, no se completó la eliminacion'
            error.fecha = datetime.now()
            error.status = 1
            db.session.add(error)
            db.session.commit()
            return 0
        else:
            datosBancarios.fecha_baja = datetime.now()
            datosBancarios.status     = -1
            db.session.commit()
            return datosBancarios.id
#-----------------------------------------------------------------------------------------------------------------------
def listaDatosBancarios(firebase_token):
    chofer  = Choferes.query.filter_by(firebase_token=computeMD5hash(firebase_token), status=1).first()
    result  = []
    partial = {}
    if chofer is None:

        error = ErroresEnSistema()

        error.error  = 'Chofer no encontrado, se detuvo la lista de datos bancarios'
        error.fecha  = datetime.now()
        error.status = 1
        db.session.add(error)
        db.session.commit()

        raise BusinessException('Chofer no encontrado, se detuvo la lista de datos bancarios')
    else:
        datosBancarios = DatosBancarios.query.filter_by(id_chofer=chofer.id,status=1).all()
        if datosBancarios is None:
            error = ErroresEnSistema()

            error.error = 'Se encontró al chofer, pero no tiene datos bancarios, no se regresa lista'
            error.fecha = datetime.now()
            error.status = 1
            db.session.add(error)
            db.session.commit()
            return result
        else:
            for datos in datosBancarios:
                partial = {
                    'id'        :datos.id,
                    'id_chofer' :datos.id_chofer,
                    'banco'     :datos.banco,
                    'cuenta'    :datos.cuenta,
                    'clabe'     :datos.clabe,
                    'fecha_alta':str(datos.fecha)
                }
                result.append(partial)
        return result
#-----------------------------------------------------------------------------------------------------------------------
def insertImagenAuto(id_chofer):
    files = {}
    files['imagen']  =''
    if 'imagen' not in request.files:
        return files

    if 'imagen' in request.files:
        file_imagen = request.files['imagen']
        filename  = secure_filename(file_imagen.filename)
        filename  = filename.split('.')
        extension = filename[1]
        destino = app.config['CHOFERES_AUTO_PATH'] + str(id_chofer)

        if not os.path.exists(destino):
            if os.access(destino, os.X_OK | os.W_OK):
                os.makedirs(destino, 0o755)
            else:
                error = ErroresEnSistema()

                error.error = 'No se tienen los permisos necesarios para guardar la imagen del auto con chofer: '+str(id_chofer)
                error.fecha = datetime.now()
                error.status = 1
                db.session.add(error)
                db.session.commit()
                return files

        # -- se renombra la imagen con formato: <id>_<fecha>.<extension> --#
        filename = str(id_chofer) + '_' + str(datetime.now().microsecond) + '.' + extension
        file_imagen.save(os.path.join(destino,filename))
        files['imagen'] = filename

    return files
#-----------------------------------------------------------------------------------------------------------------------
def insertAutoInfo(firebase_token,args):
    chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(firebase_token), status=1).first()
    result    = -1
    respuesta = ''

    if chofer is None:
        error = ErroresEnSistema()
        respuesta = 'No se encontró al chofer en servicio insertAutoInfo, '+str(firebase_token)+', no se completa el registro de auto'
        error.error = respuesta
        error.fecha = datetime.now()
        error.status = 1
        db.session.add(error)
        db.session.commit()
        return result,respuesta

    nuevo_auto = Autos()

    nuevo_auto.id_chofer    = chofer.id
    nuevo_auto.placas       = args['placas']
    nuevo_auto.color        = args['color']
    nuevo_auto.descripcion  = ''
    nuevo_auto.id_tipo_auto = 1
    nuevo_auto.modelo       = args['modelo']
    nuevo_auto.puertas      = args['puertas']
    nuevo_auto.marca        = args['marca']
    nuevo_auto.fecha        = datetime.now()
    nuevo_auto.status       = 1
    nuevo_auto.licencia     = ''
    nuevo_auto.tarjeta      = ''
    nuevo_auto.poliza       = ''
    nuevo_auto.verificacion = ''

    imagen = insertImagenAuto(chofer.id)
    if len(imagen) > 0:
        nuevo_auto.imagen = imagen['imagen']
        
    else:
        error = ErroresEnSistema()
        respuesta = 'Aviso, la imagen para el auto del chofer: '+str(firebase_token)+', no se completó'
        error.error = respuesta
        error.fecha = datetime.now()
        error.status = 1
        db.session.add(error)
        db.session.commit()


    db.session.add(nuevo_auto)
    db.session.commit()

    result = nuevo_auto.id

    return result,respuesta
#-----------------------------------------------------------------------------------------------------------------------
def getAutoInfo(firebase_token):
    chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(firebase_token), status=1).first()
    result    = -1
    respuesta = []
    partial   = {}
    if chofer is None:
        error = ErroresEnSistema()
        respuesta = 'No se encontró al chofer en servicio getAutoInfo, '+str(firebase_token)
        error.error = respuesta
        error.fecha = datetime.now()
        error.status = 1
        db.session.add(error)
        db.session.commit()
        return result,respuesta

    info_auto = Autos.query.filter_by(id_chofer=chofer.id, status=1).order_by(desc(Autos.id)).all()
    if info_auto is None or len(info_auto) == 0:
        result = -2
        return result, respuesta

        'id'              :info_auto[0].id,
        'color'           :info_auto[0].color,
        'placas'          :info_auto[0].placas,
        'imagen'          :info_auto[0].imagen,
        'modelo'          :info_auto[0].modelo,
        'puertas'         :info_auto[0].puertas,
        'tipo'            :info_auto[0].id_tipo_auto,
        'marca'           :info_auto[0].marca,
        'licencia'        :info_auto[0].licencia,
        'tarjeta'         :info_auto[0].tarjeta,
        'poliza'          :info_auto[0].poliza,
        'verificacion'    :info_auto[0].verificacion,
        'fecha_alta'      :str(info_auto[0].fecha),
        'id_chofer'       :chofer.id,
        'nombres'         :chofer.nombres,
        'apellidos'       :chofer.apellidos,
        'email'           :chofer.email,
        'telefono'        :chofer.telefono,
        'imagen_perfil'   :chofer.imagen,
        'fecha_nacimiento':str(chofer.fecha_nacimiento),
        'pais'            :chofer.pais

    respuesta.append(partial)
    result = 1

    return result, respuesta
#-----------------------------------------------------------------------------------------------------------------------
def updateAuto(firebase_token,args):
    result    = -1
    respuesta = ''

    chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(firebase_token), status=1).first()
    if chofer is None:
        respuesta = 'No se encontró al chofer con firebase: ' + str(firebase_token) + ' en servicio updateAuto'
        generarErrorDelSistema(respuesta)
        return result,respuesta

    info_auto = Autos.query.filter_by(id=args['id_auto']).first()
    if info_auto is None:
        result = -2
        respuesta = 'Se enctró a chofer con firebase: ' + str(firebase_token) + ' pero no se encontró auto para editar en servicio updateAuto'
        generarErrorDelSistema(respuesta)
        return result,respuesta


    info_auto.placas  = args['placas'] if args['placas'] is not None else info_auto.placas
    info_auto.color   = args['color'] if args['color'] is not None else info_auto.color
    info_auto.marca   = args['marca'] if args['marca'] is not None else info_auto.marca
    info_auto.puertas = args['puertas'] if args['puertas'] is not None else info_auto.puertas
    info_auto.modelo  = args['modelo'] if args['modelo'] is not None else info_auto.modelo

    imagen = insertImagenAuto(chofer.id)
    if len(imagen) > 0:
        info_auto.imagen = imagen['imagen']
    else:
        respuesta = 'Advertencia, no se pudo actualizar la imagen del auto: ' + info_auto.id + ' con chofer id: '+str(chofer.id)
        generarErrorDelSistema(respuesta)

    db.session.commit()
    result = info_auto.id
    return result,respuesta
#-----------------------------------------------------------------------------------------------------------------------
def deleteAuto(firebase_token,args):
    result    = -1
    respuesta = ''

    chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(firebase_token),status=1).first()
    if chofer is None:
        respuesta = 'No se encontró al chofer con firebase: ' + str(firebase_token) + ' en servicio deleteAuto'
        generarErrorDelSistema(respuesta)
        return result,respuesta

    auto = Autos.query.filter_by(id=args['id_auto'],status=1,id_chofer=chofer.id).first()
    if auto is None:
        respuesta = 'No se encontró el auto para el chofer: ' + str(firebase_token) + ' en servicio deleteAuto'
        generarErrorDelSistema(respuesta)
        return result,respuesta

    chofer.id_auto  = 0
    auto.fecha_baja = datetime.now()
    auto.status     = -1

    db.session.commit()
    result = auto.id
    return result, respuesta
#-----------------------------------------------------------------------------------------------------------------------
def updateStatusAsignado(firebase_token,id_auto):
    chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(firebase_token),status=1).first()
    chofer.id_auto  = id_auto
    chofer.asignado = 2
    db.session.commit()
    return True
#-----------------------------------------------------------------------------------------------------------------------
def insertDocumentosAuto(firebase_token,id_auto):
    auto = Autos.query.filter_by(id=id_auto,status=1).first()
    respuesta     = 0
    respuesta_txt = 'No se pudo subir documentos para el auto: '+str(auto.id)

    documentos = uploadDocumentosAuto(auto.id_chofer,auto.id)
    if len(documentos) > 0:
        if documentos['licencia'] != '':
            auto.licencia = documentos['licencia']
        if documentos['tarjeta'] != '':
            auto.tarjeta = documentos['tarjeta']
        if documentos['poliza'] != '':
            auto.poliza = documentos['poliza']
        if documentos['verificacion'] != '':
            auto.verificacion = documentos['verificacion']
        respuesta     = auto.id
        respuesta_txt = 'documentos cargados para auto: '+str(auto.id)
    return respuesta,respuesta_txt
#-----------------------------------------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------------------------------------