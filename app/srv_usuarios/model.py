from app import db

class Usuarios(db.Model):
    __tablename__               = 'usuarios'
    id                          = db.Column(db.Integer, primary_key=True)
    nombres                     = db.Column(db.String(100))
    apellidos                   = db.Column(db.String(100))
    email                       = db.Column(db.String(100))
    password                    = db.Column(db.String(100))
    firebase_token              = db.Column(db.String(150))
    firebase_token_notification = db.Column(db.String(150))
    token_conekta               = db.Column(db.String(150))
    sesion_unica                = db.Column(db.Integer)
    imagen                      = db.Column(db.String(100))
    ine                         = db.Column(db.String(100))
    comprobante_domicilio       = db.Column(db.String(100))
    telefono                    = db.Column(db.Integer)
    fecha_nacimiento            = db.Column(db.Date)
    fecha_actualizacion         = db.Column(db.DateTime)
    fecha                       = db.Column(db.DateTime)
    ine_status                  = db.Column(db.Integer)
    rating                      = db.Column(db.Float)
    status                      = db.Column(db.Integer)


    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def dump(self):
        return dict([('id', self.id), ('nombres ', self.nombres ), ('apellidos', self.apellidos),
                 ('email', self.email), ('password', self.password), ('status', self.status),
                 ('firebase_token', self.firebase_token), ('sesion_unica', self.sesion_unica),('imagen', self.imagen),
                 ('ine', self.ine),('telefono', self.telefono),('fecha_nacimiento', self.fecha_nacimiento),
                 ('firebase_token_notification', self.firebase_token_notification),('comprobante_domicilio', self.comprobante_domicilio),
                 ('token_conekta',self.token_conekta),('rating',self.rating),('ine_status',self.ine_status)])

class Viajes(db.Model):
    __tablename__ = 'viajes'
    id                = db.Column(db.Integer, primary_key=True)
    idusuario         = db.Column(db.Integer)
    origen_latitud    = db.Column(db.String(100))
    origen_longitud   = db.Column(db.String(100))
    origen_direccion  = db.Column(db.Text)
    destino_latitud   = db.Column(db.String(100))
    destino_longitud  = db.Column(db.String(100))
    destino_direccion = db.Column(db.Text)
    idchofer          = db.Column(db.Integer)
    orden_viaje       = db.Column(db.String(100))
    fecha             = db.Column(db.DateTime)
    status            = db.Column(db.Integer)

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def dump(self):
        return dict([ ('id',self.id), ('idusuario',self.idusuario), ('origen_latitud',self.origen_latitud),
                ('origen_longitud',self.origen_longitud), ('origen_direccion',self.origen_direccion),
                ('destino_latitud',self.destino_latitud), ('destino_longitud',self.destino_longitud),
                ('destino_direccion',self.destino_direccion), ('idchofer',self.idchoferd),('orden_viaje',self.orden_viaje)])

class Tarjetas(db.Model):
    __tablename__ = 'tarjetas_bancarias'
    id             = db.Column(db.Integer, primary_key=True)
    id_usuario     = db.Column(db.Integer)
    numero_tarjeta = db.Column(db.Integer)
    tipo_tarjeta   = db.Column(db.Integer)
    nombre_tarjeta = db.Column(db.String(500))
    fecha          = db.Column(db.DateTime)
    fecha_baja     = db.Column(db.DateTime)
    status         = db.Column(db.Integer)

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def dump(self):
        return dict([ ('id',self.id), ('id_usuario',self.id_usuario), ('numero_tarjeta',self.numero_tarjeta),
                ('tipo_tarjeta',self.tipo_tarjeta), ('nombre_tarjeta',self.nombre_tarjeta),('fecha',self.fecha),
                ('fecha_baja',self.fecha_baja) ])