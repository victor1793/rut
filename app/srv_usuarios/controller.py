from flask import request, session
from flask_restful import Resource, reqparse

from app.helpers.helper import create_success_response, computeMD5hash
from app.helpers.decorator import authenticate_request
from app.generales.service import login, logout
from app import logger, app
from app.generales.service import getUsuarioByToken, generaJsonUsuario
from app.helpers.helper import create_error_response
from app.excepciones.exception import BusinessException
from app.srv_usuarios.service import updateSesionUnica, insertUsuario, addImage, updateConektaToken,updateUsuarioByToken,solictudViaje,updateFCM,insertFCM,insertTarjetaBancaria,getTarjetasBancarias,deleteTarjetasBancarias
import json


class LoginRestService():
    def login(self,firebase_token):
        respuesta = False
        firebase_token = 'cliente_'+firebase_token
        sesion_iniciada = login(firebase_token)
        if sesion_iniciada is not None:
            respuesta = True
        return respuesta

    def logout(self,firebase_token):
        respuesta = False
        firebase_token = 'cliente_' + firebase_token
        sesion_terminada = logout(firebase_token)
        if sesion_terminada is True:
            respuesta = True
        return respuesta

class UsuarioRestService(Resource):
    '''peticion de autenticacion'''
    @authenticate_request
    def get(self,firebase_token,tipo):
        usuario_json = ''
        try:
            usuario_data = getUsuarioByToken(computeMD5hash(firebase_token),tipo)
            if usuario_data.firebase_token is not None:
                servicio_login = LoginRestService()
                servicio_login.login(usuario_data.firebase_token)
                if servicio_login is not None:
                    #actualizo el campo sesion unica en la base
                    updateSesionUnica(firebase_token,1,tipo)
                    data_usr = generaJsonUsuario(usuario_data)
                    usuario_json = {'Usuario': data_usr}
                else:
                    raise BusinessException('Error en inicio de sesion')
            else:
                raise BusinessException('El token no existe en la base')
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(usuario_json)

    @authenticate_request
    def post(self,firebase_token,tipo):
        '''Funcion encargada de insertar datos del usuario'''
        parser = reqparse.RequestParser()
        parser.add_argument('nombres',          required=True, help='parametro nombres es requerido')
        parser.add_argument('apellidos',        required=True, help='parametro apellidos es requerido')
        parser.add_argument('email',            required=True, help='parametro email es requerido')
        parser.add_argument('telefono',         required=True, help='parametro telefono es requerido')
        parser.add_argument('fecha_nacimiento', required=True, help='parametro fecha_nacimiento es requerido')
        parser.add_argument('password',         required=False,help='')

        info_usuario = parser.parse_args()
        try:
            servicio_login = LoginRestService()
            servicio_login.login(firebase_token)
            if servicio_login is not None:
                #insertamos la informacion del usuario
                usuario_id = insertUsuario(firebase_token,info_usuario)
                if usuario_id > 0:
                    # actualizo el campo sesion unica en la base
                    updateSesionUnica(firebase_token, 1,tipo)
                    usuario_json = {'Usuario Add': 'success', 'usuario_id': usuario_id}
                else:
                    usuario_json = {'Usuario Add': 'error', 'usuario_id': 0}
            else:
                raise BusinessException('Error en inicio de sesion')
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(usuario_json)

    @authenticate_request
    def patch(self,firebase_token,tipo):
        '''Funcion para eliminar la sesion del usuario (logout)'''
        try:
            logger.info('\n\n Entre a funcion purge con token: '+str(firebase_token))
            servicio_login = LoginRestService()
            respuesta = servicio_login.logout(firebase_token)
            usuario_json = {}
            if respuesta == True:
                #actualizamos el campo sesion_unica de la base de datos
                updateSesionUnica(firebase_token,0,tipo)
                usuario_json = {'Usuario':True}
            else:
                raise BusinessException('Error en retirar la sesion del usuario')
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(usuario_json)

    @authenticate_request
    def put(self,firebase_token,tipo):
        '''Funcion para actulizar un usuario en base a su token'''
        parser = reqparse.RequestParser()
        parser.add_argument('nombres',          required=False, help='')
        parser.add_argument('apellidos',        required=False, help='')
        parser.add_argument('telefono',         required=False, help='')
        parser.add_argument('fecha_nacimiento', required=False, help='')
        parser.add_argument('password',         required=False, help='')

        info_usuario = parser.parse_args()
        try:
            usuario_json = ''
            logger.info('\n\n Entre a funcion put con token: ' + str(firebase_token))
            usuario_id = updateUsuarioByToken(firebase_token,info_usuario)
            if usuario_id > 0:
                usuario_json = {'Usuario update':'success','usuario_id': usuario_id}
            else:
                usuario_json = {'Usuario update': 'error', 'usuario_id': usuario_id}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(usuario_json)



class ImagenesRestService(Resource):
    @authenticate_request
    def post(self,firebase_token,tipo):
        try:
            logger.info('\n\n Entre a servicio de imagenes: ' + str(firebase_token))
            parser = reqparse.RequestParser()
            args = parser.parse_args()
            args['firebase_token'] = firebase_token
            imagen_subida = addImage(args,tipo)
            if imagen_subida != False:
                img_json = {'Imagenes': imagen_subida}
            else:
                return create_error_response({'Imagenes': False})
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(img_json)

class ConektaTokenRestService(Resource):
    @authenticate_request
    def post(self,firebase_token,tipo):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('conektaToken', required=True, help='parametro conektaToken es requerido')
            args = parser.parse_args()
            user = updateConektaToken(firebase_token,args)
            if user > 0:
                response = {'UpdateToken':{'usuario':user}}
            else:
                response = {'UpdateToken': {'usuario':user}}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(response)

class SolicitarViajeRestService(Resource):
    @authenticate_request
    def post(self,firebase_token,tipo):

        parser = reqparse.RequestParser()
        parser.add_argument('origen_lat',  required=True, help='parametro origen_lat es requerido')
        parser.add_argument('origen_lng',  required=True, help='parametro origen_lng es requerido')
        parser.add_argument('origen_dir',  required=True, help='parametro origen_dir es requerido')
        parser.add_argument('destino_lat', required=True, help='parametro destino_lat es requerido')
        parser.add_argument('destino_lng', required=True, help='parametro destino_lng es requerido')
        parser.add_argument('destino_dir', required=True, help='parametro destino_dir es requerido')

        info_viaje = parser.parse_args()
        viaje_json = ''
        try:
            solicitud = solictudViaje(firebase_token,info_viaje)
            if solicitud > 0:
                viaje_json = {'solicitudViaje': 'success', 'solicitud_id': solicitud}
            else:
                viaje_json = {'solicitudViaje': 'error', 'solicitud_id': 0}
        except BusinessException as e:
            generaJsonUsuario('Error en clase solicitar viaje: '+str(e.message)+' ; firebase: '+str(firebase_token))
            return create_error_response(e.message)
        return create_success_response(viaje_json)


class DatosUsuariosRestService(Resource):
    @authenticate_request
    def get(self,firebase_token,tipo):
        usuario_json = ''
        try:
            usuario_data = getUsuarioByToken(computeMD5hash(firebase_token),tipo)
            if usuario_data.firebase_token is not None:
                data_usr = generaJsonUsuario(usuario_data)
                usuario_json = {'Usuario': data_usr}
            else:
                raise BusinessException('El token no existe en la base')
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(usuario_json)

class UsuarioFCMRestService(Resource):
    @authenticate_request
    def post(self,firebase_token,tipo):
        fcm_json = ''
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('fcm', required=True, help='')
            info_fcm = parser.parse_args()
            id_usuario = insertFCM(firebase_token,info_fcm)
            if id_usuario > 0:
                fcm_json = {'Usuario add fcm': 'success', 'usuario_id': id_usuario}
            else:
                fcm_json = {'Usuario add fcm': 'error', 'usuario_id': 0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(fcm_json)

    @authenticate_request
    def put(self,firebase_token,tipo):
        fcm_json = ''
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('fcm', required=True, help='parametro fcm es requerido')
            info_fcm = parser.parse_args()
            id_usuario = updateFCM(firebase_token,info_fcm)
            if id_usuario > 0:
                fcm_json = {'Usuario update fcm': 'success', 'usuario_id': id_usuario}
            else:
                fcm_json = {'Usuario update fcm': 'error', 'usuario_id': 0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(fcm_json)

class TarjeteasBancariasRestService(Resource):
    @authenticate_request
    def get(self,firebase_token,tipo):
        lista_tarjetas_json = ''
        try:
            resultado=getTarjetasBancarias(firebase_token)
            lista_tarjetas_json = {'Tarjetas':resultado}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(lista_tarjetas_json)

    @authenticate_request
    def post(self,firebase_token,tipo):
        parser = reqparse.RequestParser()
        parser.add_argument('tokenConekta' , required = True, help = 'parametro tokenConekta  es requerido')
        parser.add_argument('numeroTarjeta', required = True, help = 'parametro numeroTarjeta es requerido')
        parser.add_argument('tipoTarjeta'  , required = True, help = 'parametro tipoTarjeta   es requerido')
        parser.add_argument('nombreTarjeta', required = True, help = 'parametro nombreTarjeta es requerido')

        info_tarjetas = parser.parse_args()
        tarjetas_json = ''
        try:
            tarjeta_id = insertTarjetaBancaria(firebase_token,info_tarjetas)
            if tarjeta_id > 0:
                tarjetas_json = {'Usuario add tarjeta': 'success', 'tarjeta_id': tarjeta_id}
            else:
                tarjetas_json = {'Usuario add tarjeta': 'error', 'tarjeta_id': 0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(tarjetas_json)

    @authenticate_request
    def put(self,firebase_token,tipo):
        parser = reqparse.RequestParser()
        parser.add_argument('id_tarjeta', required=True, help='parametro id_tarjeta es requerido')

        info_tarjetas = parser.parse_args()
        tarjetas_json = ''
        try:
            tarjeta_id = deleteTarjetasBancarias(firebase_token,info_tarjetas)
            if tarjeta_id > 0:
                tarjetas_json = {'Usuario delete tarjeta': 'success', 'tarjeta_id': tarjeta_id}
            else:
                tarjetas_json = {'Usuario delete tarjeta': 'error', 'tarjeta_id': 0}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(tarjetas_json)