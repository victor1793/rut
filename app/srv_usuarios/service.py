from app import db, app, logger
from app.srv_usuarios.model import Usuarios, Viajes, Tarjetas
from app.srv_choferes.model import Choferes
from app.excepciones.exception import BusinessException
from app.helpers.helper import computeMD5hash,createOrdenViaje
from app.generales.service import uploadImage, generarErrorDelSistema, findPais
from datetime import datetime

#-----------------------------------------------------------------------------------------------------------------------
def getUsuario():
    usuario = Usuarios.query.filter_by(id=1).first()
    if not usuario:
        return None
    else:
        return usuario
#-----------------------------------------------------------------------------------------------------------------------
def updateSesionUnica(token,valor,tipo):
    if tipo == 'cliente':
        usuario = Usuarios.query.filter_by(firebase_token=computeMD5hash(token)).first()
        if usuario is None:
            raise BusinessException('Cliente no encontrado, no se pudo actualizar la sesion')
        usuario.sesion_unica = valor
        db.session().commit()
    elif tipo == 'chofer':
        chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(token)).first()
        if chofer is None:
            raise BusinessException('Chofer no encontrado, no se pudo actualizar la sesion')
        chofer.sesion_unica = valor
        db.session().commit()
    else:
        return False
#-----------------------------------------------------------------------------------------------------------------------
def insertUsuario(firebase_token,values):

    usuario = Usuarios.query.filter_by(firebase_token=computeMD5hash(firebase_token),status=1).first()

    if not usuario:
        generarErrorDelSistema('Error, no se pudo agregar la información del usuario con firebase_token:'+str(firebase_token))
        return 0


    aux = values['fecha_nacimiento'].split('-')
    data_split = str(aux[2]) + '-' + str(aux[1]) + '-' + str(aux[0])

    usuario.nombres                     = values['nombres']
    usuario.apellidos                   = values['apellidos']
    usuario.email                       = values['email']
    usuario.telefono                    = values['telefono']
    usuario.fecha_nacimiento            = data_split
    usuario.firebase_token_notification = ''
    usuario.password                    = values['password']
    usuario.token_conekta               = ''
    usuario.comprobante_domicilio       = ''
    usuario.imagen                      = ''
    usuario.ine                         = ''
    usuario.ine_status                  = 0
    usuario.rating                      = 0.0
    usuario.comprobante_status          = 0
    usuario.antecedentes_status         = 0
    db.session().commit()
    return usuario.id
#-----------------------------------------------------------------------------------------------------------------------
def addImage(args,tipo):
    #-- Funcion encargada guardar imagenes (cadenas) en la base de datos

    #--buscamos al usuario para actualizar el campo de imagen
    usuario = None
    chofer  = None
    if tipo == 'cliente':
        usuario = Usuarios.query.filter_by(firebase_token=computeMD5hash(args['firebase_token']),status=1).first()
    elif tipo == 'chofer':
        chofer = Choferes.query.filter_by(firebase_token=computeMD5hash(args['firebase_token']), status=1).first()

    respuesta = {}
    respuesta['ine']          = ''
    respuesta['comprobante']  = ''
    respuesta['antecedentes'] = ''
    respuesta['profile']      = ''
    if usuario is not None:
        uploads = uploadImage(usuario.id, 'usuario')

        if uploads is False:
            return False

        if uploads['ine'] != "":
            usuario.ine      = uploads['ine']
            respuesta['ine'] = app.config['USERS_INE_PATH'] + str(usuario.id) + '/' + uploads['ine']
        if uploads['comprobante'] != "":
            usuario.comprobante_domicilio = uploads['comprobante']
            respuesta['comprobante']      = app.config['USERS_COMPROBANTE_DOMICILIO'] + str(usuario.id) + '/' + uploads['comprobante']
        if uploads['profile'] != "":
            usuario.imagen       = uploads['profile']
            respuesta['profile'] = app.config['USERS_PROFILE_PATH'] + str(usuario.id) + '/' + uploads['profile']

        usuario.fecha_actualizacion = datetime.now()
        db.session().commit()
        return respuesta
    elif chofer is not None:
        uploads = uploadImage(chofer.id, 'chofer')

        if uploads is False:
            return False

        if uploads['ine'] != "":
            chofer.ine      = uploads['ine']
            respuesta['ine'] = app.config['CHOFERES_INE_PATH'] + str(chofer.id) + '/' + uploads['ine']
        if uploads['comprobante'] != "":
            chofer.comprobante_domicilio = uploads['comprobante']
            respuesta['comprobante']      = app.config['CHOFERES_COMPROBANTE_DOMICILIO'] + str(chofer.id) + '/' + uploads['comprobante']
        if uploads['profile'] != "":
            chofer.imagen       = uploads['profile']
            respuesta['profile'] = app.config['CHOFERES_PROFILE_PATH'] + str(chofer.id) + '/' + uploads['profile']
        if uploads['antecedentes'] != '':
            chofer.antecedentes = uploads['antecedentes']
            respuesta['antecedentes'] = app.config['CHOFERES_CARTAS_ANTECEDENTES_PATH'] + str(chofer.id) + '/' + uploads['antecedentes']

        chofer.fecha_actualizacion = datetime.now()
        db.session().commit()
        return respuesta
    else:
        return False
#-----------------------------------------------------------------------------------------------------------------------
def updateConektaToken(firebase_token,args):
    #--Funcion encargada de actualizar solo el token de conekta
    usuario = Usuarios.query.filter_by(firebase_token=computeMD5hash(firebase_token),status=1).first()
    if usuario:
        usuario.token_conekta       = args['conektaToken']
        usuario.fecha_actualizacion = datetime.now()
        db.session().commit()
        return usuario.id
    return 0
#-----------------------------------------------------------------------------------------------------------------------
def updateUsuarioByToken(firebase_token,args):
    #-- Funcion encargada de actualizar los datos del usuario en base al token
    usuario = Usuarios.query.filter_by(firebase_token=computeMD5hash(firebase_token),status=1).first()
    if usuario is None:
        raise BusinessException('Usuario no encontrado, no se actualizo la informacion')
    else:


        usuario.nombres          = args['nombres'] if args['nombres'] is not None else usuario.nombres
        usuario.apellidos        = args['apellidos'] if args['apellidos'] is not None else usuario.apellidos
        usuario.password         = args['password'] if args['password'] is not None else usuario.password
        usuario.telefono         = args['telefono'] if args['telefono'] is not None else usuario.telefono
        data_split = ''
        if args['fecha_nacimiento'] != '':
            aux = args['fecha_nacimiento'].split('-')
            data_split = str(aux[2]) + '-' + str(aux[1]) + '-' + str(aux[0])
            usuario.fecha_nacimiento = data_split

        db.session().commit()
        return usuario.id
#-----------------------------------------------------------------------------------------------------------------------
def solictudViaje(firebase_token,args):
    usuario = Usuarios.query.filter_by(firebase_token = computeMD5hash(firebase_token),status=1).first()
    if usuario is None:
        generarErrorDelSistema('Error en solicitud de viaje, no se encontró al usuario con firebase_token: ' + str(firebase_token) )
        return 0
    else:
        #-----------------comienza logica de viaje----------------------#
        # -primero se busca el pais del cliente de acuerdo a su latitud y longitud
        coordenadas = str(args['origen_lat'])+','+str(args['origen_lng'])
        pais = findPais(coordenadas)
        #-Se buscan a los choferes mas cercanos de acuerdo al pais
        choferes = 0



        nuevo_viaje = Viajes()
        orden_viaje = createOrdenViaje()

        nuevo_viaje.idusuario         = usuario.id
        nuevo_viaje.origen_latitud    = args['origen_lat']
        nuevo_viaje.origen_longitud   = args['origen_lng']
        nuevo_viaje.origen_direccion  = args['origen_dir']
        nuevo_viaje.destino_latitud   = args['destino_lat']
        nuevo_viaje.destino_longitud  = args['destino_lng']
        nuevo_viaje.destino_direccion = args['destino_dir']
        nuevo_viaje.idchofer          = 0
        nuevo_viaje.orden_viaje       = orden_viaje
        nuevo_viaje.fecha             = datetime.now()
        nuevo_viaje.status            = 1

        db.session.add(nuevo_viaje)
        db.session.commit()



        return nuevo_viaje.id
#-----------------------------------------------------------------------------------------------------------------------
def updateFCM(firebase_token,args):
    usuario = Usuarios.query.filter_by(firebase_token = computeMD5hash(firebase_token),status=1).first()
    if usuario is None:
        raise BusinessException('Usuario no encontrado, se detuvo la actualizacion de fcm')
    else:
        usuario.firebase_token_notification = args['fcm']
        db.session.commit()
        return usuario.id
#-----------------------------------------------------------------------------------------------------------------------
def insertFCM(firebase_token,args):
    usuario = Usuarios.query.filter_by(firebase_token = computeMD5hash(firebase_token),status=1).first()
    if usuario is None:
        raise BusinessException('Usuario no encontrado, se detuvo la insercion de fcm')
    else:
        usuario.firebase_token_notification = args['fcm']
        db.session.commit()
        return usuario.id
#-----------------------------------------------------------------------------------------------------------------------
def insertTarjetaBancaria(firebase_token,args):
    usuario = Usuarios.query.filter_by(firebase_token=computeMD5hash(firebase_token), status=1).first()
    if usuario is None:
        raise BusinessException('Usuario no encontrado, se detuvo la insercion de tarjeta bancaria')
    else:
        nueva_tarjeta_bancaria = Tarjetas()

        nueva_tarjeta_bancaria.id_usuario     = usuario.id
        nueva_tarjeta_bancaria.numero_tarjeta = args['numeroTarjeta']
        nueva_tarjeta_bancaria.tipo_tarjeta   = args['tipoTarjeta']
        nueva_tarjeta_bancaria.nombre_tarjeta = args['nombreTarjeta']
        nueva_tarjeta_bancaria.fecha          = datetime.now()
        nueva_tarjeta_bancaria.status         = 1

        db.session.add(nueva_tarjeta_bancaria)
        db.session.commit()
        return nueva_tarjeta_bancaria.id
#-----------------------------------------------------------------------------------------------------------------------
def getTarjetasBancarias(firebase_token):
    usuario = Usuarios.query.filter_by(firebase_token=computeMD5hash(firebase_token), status=1).first()
    '''Respuesta default'''
    result  = []
    partial = {}
    if usuario is None:
        raise BusinessException('Usuario no encontrado, se detuvo la obtencion de tarjetas bancarias')

    lista_tarjetas_bancarias = Tarjetas.query.filter_by(id_usuario=usuario.id,status=1).all()
    '''Si encuentra tarjetas relacionadas al usuario se genera la respuesta'''
    if len(lista_tarjetas_bancarias) > 0:
        for tarjeta in lista_tarjetas_bancarias:
            partial = {
                'id'            : tarjeta.id,
                'id_usuario'    : tarjeta.id_usuario,
                'numero_tarjeta': tarjeta.numero_tarjeta,
                'tipo_tarjeta'  : tarjeta.tipo_tarjeta,
                'nombre_tarjeta': tarjeta.nombre_tarjeta
            }
            result.append(partial)
    return result
#-----------------------------------------------------------------------------------------------------------------------
def deleteTarjetasBancarias(firebase_token,args):
    usuario = Usuarios.query.filter_by(firebase_token=computeMD5hash(firebase_token), status=1).first()

    if usuario is None:
        raise BusinessException('Usuario no encontrado, se detuvo la actualizacion de tarjetas bancarias')

    info_tarjeta = Tarjetas.query.filter_by(id_usuario=usuario.id,id=args['id_tarjeta'],status=1).first()
    if info_tarjeta is not None:
        info_tarjeta.status     = -1
        info_tarjeta.fecha_baja = datetime.now()
        db.session.commit()
        return info_tarjeta.id
    else:
        return 0
#-----------------------------------------------------------------------------------------------------------------------