from flask import Flask, render_template, request, json, redirect, url_for
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail

import os
import logging

'''Logger'''
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(filename)s:%(lineno)s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M')
logger = logging.getLogger(__name__)

'''Define the WSGI application object'''
app = Flask(__name__)

'''Configurations loaded from config.py file'''
app.config.from_object('config')

'''Configurations loaded from config.py file'''
mail = Mail(app)

'''Define the api object for rest endpoints'''
api = Api(app)

'''Conexion con Mysql'''
db = SQLAlchemy(app)

'''Servicios para la aplicacion'''
from app.srv_usuarios.controller import UsuarioRestService,ImagenesRestService, ConektaTokenRestService,SolicitarViajeRestService,DatosUsuariosRestService,UsuarioFCMRestService, TarjeteasBancariasRestService
from app.auth.controller import FirebaseTokenRestServive
from app.srv_choferes.controller import Ubicaciones, ChoferRestService,DatosChoferRestService,ChoferFCMRestService,DatosBancariosRestService,AutosRestService
from app.generales.controller import ListaAutosRestService,PreguntasFrecuentes, Estados, NotificacionesRestService

api.add_resource(UsuarioRestService, '/api/getUsuario','/api/logout','/api/insertUsuario','/api/updateUsuarioByToken')
api.add_resource(FirebaseTokenRestServive, '/api/addFirebaseToken')
api.add_resource(ImagenesRestService, '/api/addImagenes')
api.add_resource(ConektaTokenRestService, '/api/updateConektaToken')
api.add_resource(SolicitarViajeRestService,'/api/solicitudViaje')
api.add_resource(Ubicaciones,'/api/updateUbicacion')
api.add_resource(ChoferRestService,'/api/updateChofer','/api/addChofer')
api.add_resource(DatosUsuariosRestService,'/api/getUsuarioInfo')
api.add_resource(DatosChoferRestService,'/api/getChoferInfo')
api.add_resource(UsuarioFCMRestService,'/api/updateFCMUsuario','/api/insertFCMUsuario')
api.add_resource(ChoferFCMRestService,'/api/updateFCMChofer','/api/insertFCMChofer')
api.add_resource(TarjeteasBancariasRestService,'/api/insertTarjetaBancaria','/api/getTarjetas','/api/deleteTarjetas')
api.add_resource(ListaAutosRestService,'/api/getListaAutos')
api.add_resource(PreguntasFrecuentes,'/api/getFaqs')
api.add_resource(Estados,'/api/getEstados')
api.add_resource(DatosBancariosRestService,'/api/insertDatosBancarios','/api/updateDatosBancarios','/api/deleteDatosBancarios','/api/getDatosBancarios')
api.add_resource(AutosRestService,'/api/insertAuto','/api/getAutoInfo','/api/updateAuto','/api/deleteAuto','/api/insertDocumentosAuto')
api.add_resource(NotificacionesRestService,'/api/sendNotificacion')

'''Servicios para la pagina web'''
from web.generales.services import addUserFromWeb

'''Ruta de bienvenida al servidor'''
@app.route('/')
def index():
    return render_template('template_site.html')

@app.route('/showSignUp')
def showSignUp():
    success = 0
    return render_template('login_user.html',value = success)

@app.route('/signUp',methods=['POST'])
def signUp():
    args = {}
    args['nombre']   = request.form['inputName']
    args['email']    = request.form['inputEmail']
    args['password'] = request.form['inputPassword']
    args['telefono'] = request.form['inputTelefono']
    args['fecha_nacimiento'] = request.form['inputFechaNacimiento']
    success = '0'

    if args['nombre'] and args['email'] and args['password'] and args['telefono'] and args['fecha_nacimiento']:
        usuario_id = addUserFromWeb(args)
        if usuario_id > 0:
            success = str(usuario_id)
    return success