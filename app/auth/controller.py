'''Esta script se encarga de recibir el firebase_token justo cuando
se termina de hacer la autenticación con firebase...
La aplicación envia el token y el script lo guarda en la base de datos
para su posterior uso.'''

from flask_restful import Resource, reqparse
from app.excepciones.exception import BusinessException
from app.helpers.helper import create_error_response, create_success_response, computeMD5hash
from app.generales.service import addFirebaseToken, generarErrorDelSistema


#Se define variable parser que sera usada en la funcion post
parser = reqparse.RequestParser()
parser.add_argument('firebase_token', required = True, help = 'parametro firebase_token es requerido')
parser.add_argument('tipo', required = True, help = 'parametro tipo_token es requerido')
class FirebaseTokenRestServive(Resource):


    def post(self):
        try:
            args = parser.parse_args()
            id_usuario_nuevo = addFirebaseToken(args['firebase_token'],args['tipo'])
            if id_usuario_nuevo > 0:
                usuario_json = {'Usuario nuevo':'success', 'usuario_id':id_usuario_nuevo}
            else:
                generarErrorDelSistema('ERROR AL AGREGAR UN USUARIO NUEVO EN addFirebaseToken, clase: '+str(args['firebase_token']) +' tipo:'+str(args['tipo']))
                usuario_json = {'Usuario nuevo': 'error', 'usuario_id': 0}
        except BusinessException as e:
            generarErrorDelSistema('Error al agregar un usuario nuevo en clase firebasetoken: '+str(e.message))
            return create_error_response(e.message)
        return create_success_response(usuario_json)