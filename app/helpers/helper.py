from app import logger
from app.generales.service import getUsuarioByToken
from app.excepciones.exception import BusinessException, AuthorizationException


import logging
import hmac
import hashlib
import base64
import random
import string

def computeMD5hash(my_string):
    '''Funcion encriptadora'''
    m = hashlib.md5()
    m.update(my_string.encode('utf-8'))
    return m.hexdigest()


def get_auth_data_from_request(header):
    '''Funcion que valida y extrae el contenido del header Authentication'''
    if not header:
        raise AuthorizationException('El header de autenticacion no fue enviado')

    data_split = header.split(":")
    size = len(data_split)

    if size != 2:
        raise AuthorizationException('Formato invalido de Authentication header')

    authentication = {
        'algorithm'      : data_split[0],
        'firebase_token' : data_split[1]
    }

    return authentication

def validate_hmac(algorithm, firebase_token,request_method,tipo):
    """Funcion que valida el digest basado en los parametros obtenidos del request"""

    #hmac es el unico algoritmo soportado
    if algorithm == "hmac":
        try:
            user = getUsuarioByToken(computeMD5hash(firebase_token),tipo)
        except Exception as e:
            logger.info('\n\n Veamos excepcion: ' + str(e) + '\n\n')
            raise BusinessException('Error trying to validate firebase_token. Try again later')
        if user is None:
            raise AuthorizationException('No se encontro el usuario, revise el token: '+str(firebase_token))
            return None

        elif not user.firebase_token:
            raise AuthorizationException('Firebase token not found for user')
        #generamos hash local para comparacion
        #dig = hmac.new(bytearray(user.firebase_token,"ASCII"), bytearray(uri,"ASCII"), hashlib.sha256).hexdigest()
    else:
        raise BusinessException('Alghoritm not supported')

    return True

def create_success_response(payload):
    '''Funcion que construye el response cuando ocurre un mensaje satisfactorio'''
    response = {
        'response': {
            'type': 'success'
        },
        'payload': payload
    }
    return response

def create_error_response(message, http_status=200):
    '''Funcion que construye el response cuando ocurre un error coloca por default el http code 200'''
    response = {
        'response': {
            'type': 'error',
            'message': message
        }
    }
    return response, http_status


def createOrdenViaje():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))

