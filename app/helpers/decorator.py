from app.helpers.helper import create_error_response, get_auth_data_from_request, validate_hmac
from app.excepciones.exception import BusinessException, AuthorizationException
from app import logger
import logging
from flask import request
from functools import wraps

def authenticate_request(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        ''' Decorator servira para auntenticar cualquier peticion protegida usando el header
        Authentication como parametro donde debe venir con el siguiente formato:
        Authentication: hmac USER:[RANDOM_NUMBER][DIGEST] '''

        try:
            header = request.headers.get('Authentication')
            header_tipo = request.headers.get('tipo')
            auth_data = get_auth_data_from_request(header)
            tipo_data = header_tipo

            print("\n\nVEAMOS EL HEADER: \n\n"+str(tipo_data))

            authenticated = validate_hmac(auth_data['algorithm'], auth_data['firebase_token'],request.method,tipo_data)
            if not authenticated:
                raise AuthorizationException('Authorization header no valido')

            kwargs['firebase_token'] = auth_data['firebase_token']
            kwargs['tipo'] = tipo_data
            logger.info('\n\n Veamos kwargs: '+str(kwargs)+'\n\n')
        except AuthorizationException as e:
            logger.info('\n\n AuthorizationException: \n\n')
            logging.exception(e)
            return create_error_response(e.message, 401)
        except BusinessException as e:
            logger.info('\n\n BusinessException: \n\n')
            logging.exception(e)
            return create_error_response(e.message)

        return f(*args, **kwargs)
    return decorated
