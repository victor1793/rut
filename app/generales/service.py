from flask import session,request
from app import logger, db,app
from app.srv_usuarios.model import Usuarios
from app.srv_choferes.model import Choferes, Autos, UbicacionChoferes
from app.generales.model import FAQS, ErroresEnSistema
from datetime import datetime
from werkzeug.utils import secure_filename
from app.helpers.utilities import COSTA_RICA_CIUDADES, MEXICO_ESTADOS, GENERAL_CIUDADES
from os.path import isfile, join
import hashlib
import os
import requests
import math
from geopy.geocoders import Nominatim
import ast
#-----------------------------------------------------------------------------------------------------------------------
def getUsuarioByToken(token,tipo):
    '''Funcion que retorna un usuario dependiendo su token'''
    # result = []
    if tipo == 'cliente':
        usuario = Usuarios.query.filter_by(firebase_token=token, status=1).first()
        return usuario
    elif tipo == 'chofer':
        chofer  = Choferes.query.filter_by(firebase_token=token, status=1).first()
        return chofer
    else:
        return None
#-----------------------------------------------------------------------------------------------------------------------
def generaJsonUsuario(usuario_info):
    partial = {
        'id'            : usuario_info.id,
        'nombres'       : usuario_info.nombres,
        'apellidos'     : usuario_info.apellidos,
        'email'         : usuario_info.email,
        'firebase_token': usuario_info.firebase_token,
        'imagen'        : usuario_info.imagen,
        'ine'           : usuario_info.ine,
        'sesion_unica'  : usuario_info.sesion_unica,
        'nacimiento'    : str(usuario_info.fecha_nacimiento),
        'rating'        : usuario_info.rating,
        'telefono'      : usuario_info.telefono,
        'status_ine'    : usuario_info.ine_status,
        'token_fcm'     : usuario_info.firebase_token_notification,
        'status'        : usuario_info.status
    }
    return partial
#-----------------------------------------------------------------------------------------------------------------------
def generaJsonChofer(chofer_info):
    partial = {
        'id'            : chofer_info.id,
        'nombres'       : chofer_info.nombres,
        'apellidos'     : chofer_info.apellidos,
        'email'         : chofer_info.email,
        'firebase_token': chofer_info.firebase_token,
        'imagen'        : chofer_info.imagen,
        'ine'           : chofer_info.ine,
        'sesion_unica'  : chofer_info.sesion_unica,
        'nacimiento'    : str(chofer_info.fecha_nacimiento),
        'rating'        : chofer_info.rating,
        'telefono'      : chofer_info.telefono,
        'status_ine'    : chofer_info.ine_status,
        'status_antecedentes': chofer_info.antecedentes_status,
        'status_comprobante' : chofer_info.comprobante_status,
        'token_fcm'     : chofer_info.firebase_token_notification,
        'asignado'      : chofer_info.id_auto,
        'status'        : chofer_info.status
    }
    return partial
#-----------------------------------------------------------------------------------------------------------------------
def login(token):
    '''funcion encargada de realizar el login del usuario en base al token'''
    session[token] = token
    logger.info('\n\n\nVeamos sesion token: ' + str(session.get(token)))
    # if session.get(token) == True:
    # logger.info('\n\n\nEntramos al if session.get: '+str(session[token])+'\n\n\n')
    return str(session.get(token))
    # else:
    # logger.info('\n\n\nEntramos al ELSE: \n\n\n')
    # return None
#-----------------------------------------------------------------------------------------------------------------------
def logout(token):
    '''Funcion encargada de cerrar la sesion del usuario en base al token'''
    respuesta = False
    logger.info('\n\n\nVeamos antes: ' + str(session.get(token)) + '\n\n\n')
    session.pop(token, None)
    logger.info('\n\n\nEntramos al logout del general: ' + str(session.get(token)) + '\n\n\n')
    if session.get(token) is None:
        logger.info('\n\n\nEntramos al if del general: \n\n\n')
        respuesta = True
    return respuesta
#-----------------------------------------------------------------------------------------------------------------------
def addFirebaseToken(firebase_token,tipo):
    m = hashlib.md5()
    m.update(firebase_token.encode('utf-8'))
    nuevo_token = m.hexdigest()

    if tipo == 'cliente':
        #--Para evitar que se agregue el mismo firebase_token, lo buscamos en la base de datos, si se encuentra
        #--simplemente regresamos el id correspondiente al cliente, de lo contrario se hace agrega
        existe_firebase = Usuarios.query.filter_by(firebase_token=nuevo_token,status=1).first()
        if existe_firebase is not None:
            return existe_firebase.id
        else:
            usuario_nuevo = Usuarios()
            usuario_nuevo.nombres        = ""
            usuario_nuevo.apellidos      = ""
            usuario_nuevo.email          = ""
            usuario_nuevo.password       = ""
            usuario_nuevo.firebase_token = nuevo_token
            usuario_nuevo.imagen         = ""
            usuario_nuevo.ine            = ""
            usuario_nuevo.sesion_unica   = 0
            usuario_nuevo.fecha          = datetime.now()
            usuario_nuevo.status         = 1

            db.session.add(usuario_nuevo)
            db.session.commit()
            return usuario_nuevo.id

    elif tipo == 'chofer':

        existe_firebase = Choferes.query.filter_by(firebase_token=nuevo_token,status=1).first()
        if existe_firebase is not None:
            return existe_firebase.id
        else:
            chofer_nuevo = Choferes()
            chofer_nuevo.nombres        = ""
            chofer_nuevo.apellidos      = ""
            chofer_nuevo.email          = ""
            chofer_nuevo.password       = ""
            chofer_nuevo.firebase_token = nuevo_token
            chofer_nuevo.imagen         = ""
            chofer_nuevo.ine            = ""
            chofer_nuevo.sesion_unica   = 0
            chofer_nuevo.fecha          = datetime.now()
            chofer_nuevo.status         = 1

            db.session.add(chofer_nuevo)
            db.session.commit()
            return chofer_nuevo.id
    else:
        return 0
#-----------------------------------------------------------------------------------------------------------------------
def uploadImage(id, tipoDeUsuario):
    #-- servicio para subir una o varias imagenes al proyecto
    #--

    if 'comprobante' not in request.files and 'ine' not in request.files and 'antecedentes' not in request.files and \
            'profile' not in request.files:
        return False

    files = {}
    files['comprobante']  =''
    files['ine']          = ''
    files['antecedentes'] = ''
    files['profile']      = ''

    if 'comprobante' in request.files:
        file_comprobante = request.files['comprobante']
        filename  = secure_filename(file_comprobante.filename)
        filename  = filename.split('.')
        extension = filename[1]
        if tipoDeUsuario == 'usuario':
            destino = app.config['USERS_COMPROBANTE_DOMICILIO'] + str(id)
        elif tipoDeUsuario == 'chofer':
            destino = app.config['CHOFERES_COMPROBANTE_DOMICILIO'] + str(id)
        if not os.path.exists(destino):
            os.makedirs(destino, 0o755)
        # -- se renombra la imagen con formato: <id>_<fecha>.<extension> --#
        filename = str(id) + '_' + str(datetime.now().microsecond) + '.' + extension
        # -se guarda la imagen
        file_comprobante.save(os.path.join(destino, filename))
        files['comprobante'] = filename

    if 'ine' in request.files:
        file_ine = request.files['ine']
        filename_ine  = secure_filename(file_ine.filename)
        filename_ine  = filename_ine.split('.')
        extension = filename_ine[1]
        if tipoDeUsuario == 'usuario':
            destino = app.config['USERS_INE_PATH'] + str(id)
        elif tipoDeUsuario == 'chofer':
            destino = app.config['CHOFERES_INE_PATH'] + str(id)
        if not os.path.exists(destino):
            os.makedirs(destino, 0o755)
        # -- se renombra la imagen con formato: <id>_<fecha>.<extension> --#
        filename_ine = str(id) + '_' + str(datetime.now().microsecond) + '.' + extension
        # -se guarda la imagen
        file_ine.save(os.path.join(destino, filename_ine))
        files['ine'] = filename_ine

    if 'antecedentes' in request.files:
        file_antecedentes = request.files['antecedentes']
        filename_antecedentes  = secure_filename(file_antecedentes.filename)
        filename_antecedentes  = filename_antecedentes.split('.')
        extension = filename_antecedentes[1]
        destino = app.config['CHOFERES_PROFILE_PATH'] + str(id)
        if not os.path.exists(destino):
            os.makedirs(destino, 0o755)
        # -- se renombra la imagen con formato: <id>_<fecha>.<extension> --#
        filename_antecedentes = str(id) + '_' + str(datetime.now().microsecond) + '.' + extension
        # -se guarda la imagen
        file_antecedentes.save(os.path.join(destino, filename_antecedentes))
        files['antecedentes'] = filename_antecedentes

    if 'profile' in request.files:
        file_profile = request.files['profile']
        filename_profile  = secure_filename(file_profile.filename)
        filename_profile  = filename_profile.split('.')
        extension = filename_profile[1]
        if tipoDeUsuario == 'usuario':
            destino = app.config['USERS_PROFILE_PATH'] + str(id)
        elif tipoDeUsuario == 'chofer':
            destino = app.config['CHOFERES_PROFILE_PATH'] + str(id)
        if not os.path.exists(destino):
            os.makedirs(destino, 0o755)
        # -- se renombra la imagen con formato: <id>_<fecha>.<extension> --#
        filename_profile = str(id) + '_' + str(datetime.now().microsecond) + '.' + extension
        # -se guarda la imagen
        file_profile.save(os.path.join(destino, filename_profile))
        files['profile'] = filename_profile
    return files
#-----------------------------------------------------------------------------------------------------------------------
def getListaAutos():
    autos = Autos.query.filter_by(status=1).all()
    result  = []
    partial = {}
    if autos is not None:
        for car in autos:
            partial = {
                'id'                : car.id,
                'id_chofer'         : car.id_chofer,
                'id_tipo_auto'      : car.id_tipo_auto,
                'marca'             : car.marca,
                'color'             : car.color,
                'modelo'            : car.modelo,
                'imagen'            : car.imagen,
                'placas'            : car.placas,
                'puertas'           : car.puertas
            }
            result.append(partial)
    return result
#-----------------------------------------------------------------------------------------------------------------------
def getFAQS(tipo):
    result  = []
    partial = {}
    faqs = FAQS.query.filter_by(status=1,tipo=tipo).all()
    for pregunta in faqs:
        partial = {
            'id'           :pregunta.id,
            'pregunta_es'  :pregunta.pregunta_es,
            'pregunta_eng' :pregunta.pregunta_eng,
            'respuesta_es' :pregunta.respuesta_es,
            'respuesta_eng':pregunta.respuesta_eng,
            'tipo'         :pregunta.tipo
        }
        result.append(partial)

    return result
#-----------------------------------------------------------------------------------------------------------------------
def getEstados(tipo):
    result = ''
    if tipo == 'mx':
        result = MEXICO_ESTADOS

    elif tipo == 'cr':
        result = COSTA_RICA_CIUDADES
    else:
        result = GENERAL_CIUDADES

    return result
#-----------------------------------------------------------------------------------------------------------------------
def generarErrorDelSistema(mensaje):
    '''Funcion encargada de guardar el error provocado en la aplicacion movil'''
    error = ErroresEnSistema()
    error.error  = mensaje
    error.fecha  = datetime.now()
    error.status = 1
    db.session.add(error)
    db.session.commit()
#-----------------------------------------------------------------------------------------------------------------------
def createNotificacionFCM(firebase_token,tipo):
    usuario = None
    chofer  = None
    fcm     = ''
    respuesta_status = 0 #No se envia notificacion
    respuesta_str    = ''
    click_action     = ''
    body             = ''
    title            = ''
    sound            = ''
    conductor        = ''
    pasajero         = ''

    '''Encriptador'''
    m = hashlib.md5()
    m.update(firebase_token.encode('utf-8'))
    token = m.hexdigest()

    if tipo == 'cliente':
        usuario = Usuarios.query.filter_by(firebase_token=token,status=1).first()
    elif tipo == 'chofer':
        chofer  = Choferes.query.filter_by(firebase_token=token,status=1).first()
    else:
        generarErrorDelSistema('Advertencia se está enviando un tipo distinto al esperado: '+str(tipo)+' en servicio createNotificacionFCM')
        return False

    if usuario is not None:
        title        = 'Viaje Aceptado'
        sound        = 'aceptar'
        body         = 'Alguien acepto tu viaje'
        click_action = '.Trip'
        fcm          = usuario.firebase_token_notification
        pasajero     = usuario.nombres +' '+ usuario.apellidos
    elif chofer is not None:
        title        = 'Solicitud de Viaje'
        sound        = 'solicitar'
        body         = 'Alguien necesita que lo lleves'
        click_action = '.TripIncoming'
        fcm          = chofer.firebase_token_notification
        conductor    = chofer.nombres +' '+  chofer.apellidos
    else:

        return respuesta_status, respuesta_str

    if fcm == '':
        respuesta_status = -2 #sin fcm, no existe en la tabla
        respuesta_str    = 'No se encontró un fcm para: '+str(firebase_token)+' tipo:'+str(tipo)
        return respuesta_status, respuesta_str

    partial = {
        "to": fcm,
        "notification": {
            "click_action": click_action,
            "body" : body,
            "title": title,
            "sound": sound
        },
        "data": {
            "tipo"  : sound,
            "origen": {
                "longitud": -99.9,
                "latitud" : 19.9
            },
            "destino": {
                "longitud": -98.8,
                "latitud" : 19.9
            },
            "conductor": conductor,
            "pasajero" : pasajero
        }
    }

    url = "https://fcm.googleapis.com/fcm/send"

    payload = "{\n\t\"to\": \""+fcm+"\",\n\n\t\"notification\": {\n\t\t\"click_action\" : \""+click_action+"\",\n\t\t\"body\": \""+body+"\",\n\t\t\"title\": \""+title+"\",\n\t\t\"sound\": \""+sound+"\"\n\t},\n\t\"data\": {\n\t\t\"tipo\": \""+sound+"\",\n\t\t\"origen\": {\"latitud\":19.406857,\"longitud\":-99.151321},\n\t\t\"destino\": {\"latitud\":19.388803,\"longitud\":-99.129048},\n\t\t\"conductor\": \""+conductor+"\",\n\t\t\"pasajero\": \""+pasajero+"\"\n\t}\n}"
    headers = {
        'Authorization': "key=AAAAhmlP7ow:APA91bGvhp36ronSax_q9GXBMqkHPHle_eodIMvbMXlTEqL9kWH3lqrU-b568Sb6VS-Lr3lDsDYD9aJ85kLa6oyL6XUp4aK1x6ZRrpwBcHuwcZkI7UYjFrn0D6YBsQ7To5Yi1AzDMIsR2zHlOY2ouPtPD_sxr90Rpg",
        'Content-Type': "application/json",
        'Cache-Control': "no-cache",
        'Postman-Token': "d92cfaf1-2521-420d-8c94-5287fa2c768a"
    }

    response = requests.request("POST", url, data=payload, headers=headers)
    print("\n\n\nVeamos response: "+str(response.text))

    return respuesta_status,respuesta_str
# ----------------------------------------------------------------------------------------------------------------------
def haversine(lat1,lon1,lat2,lon2):
    radianes = math.pi / 180  #conversion de radianes
    R        = 6372.795477598 #Radio de la tierra en kilometros
    delta_latitud  = lat2 - lat1
    delta_longitud = lon2 - lon1
    producto = math.sin(delta_latitud * radianes/2)**2 + math.cos(radianes * lat1)*math.cos(radianes * lat2) * math.sin(radianes * delta_longitud/2)**2
    distancia = 2 * R * math.asin(math.sqrt(producto))
    distancia = round(distancia,2)
    return distancia
# ----------------------------------------------------------------------------------------------------------------------
def googleMatrixAPI(origen,destino):
    '''
    Funcion encargada de consultar la API de google, se requiere un API_KEY el cual es:
    AIzaSyAiLN2Ho1ZndbgimpFgcHGGzJwd6PGSZSE
    la cual está ligada al proyecto de rut en firebase
    La API retorna un diccionario con la distancia recorrida en kilometros y el tiempo aproximado de la ruta
    '''
    url     = "https://maps.googleapis.com/maps/api/distancematrix/json"
    origen  = str(origen)  #latitud,longitud
    destino = str(destino) #latitud,longitud
    querystring = {
        "origins"     : origen,
        "destinations": destino,
        "key"         : "AIzaSyAiLN2Ho1ZndbgimpFgcHGGzJwd6PGSZSE"
    }
    headers = {
        'Cache-Control': "no-cache",
        'Postman-Token': "bf435450-ae34-4f01-ba6d-9831d16dbeee"
    }
    response = requests.request("GET", url, headers=headers, params=querystring)
    #validamos el status de la respuesta (son en total 7: OK,INVALID_REQUEST,MAX_ELEMENTS_EXCEEDED,OVER_DAILY_LIMIT,OVER_QUERY_LIMIT,REQUEST_DENIED,UNKNOWN_ERROR)
    datos = ast.literal_eval(response.text)
    if datos['status'] != 'OK':
        return []

    return datos
# ----------------------------------------------------------------------------------------------------------------------
def findChoferesMasCercanos(args='',pais='',limite_km=3):
    '''
    Funcion encargada de buscar a los choferes mas cercanos acorde a la posición del cliente. Por default se buscará
    en un radio de 3 kilometros, si no se llega a encontrar choferes se puede ampliar el rango pasando como parametro
    el numero de kilometros dentro de la variable llamada limite_km, se puede ampliar desde 3,5,7 kilometros
    '''
    respuesta = []
    partial   = {}
    respuesta_str = ''

    #-Buscamos a los choferes usando los filtros de pais, status y asignado
    lista_choferes_por_pais = Choferes.query.filter_by(status=1,pais='mx',asignado=1).all()
    if lista_choferes_por_pais is None:
        respuesta_str = 'No se encontraron choferes para el pais: '+str(pais)+' con coordenadas:'+str(args)+' en un rango de '+str(limite_km)+'Kilometros'
        generarErrorDelSistema(respuesta_str)
        return respuesta,respuesta_str

    lista_ubicacion_choferes = []
    for lista in lista_choferes_por_pais:
        ubicacion = UbicacionChoferes.query.filter_by(idchofer=lista.id,status=1).first()
        if ubicacion is not None:
            partial = {
                'id'      :ubicacion.id,
                'idchofer':ubicacion.idchofer,
                'latitud' :ubicacion.latitud,
                'longitud':ubicacion.longitud
            }
            lista_ubicacion_choferes.append(partial)
    if len(lista_ubicacion_choferes) == 0:
        respuesta_str = 'No hay ubicaciones de los choferes para el pais: '+str(pais)+' con coordenadas:'+str(args)+' en un rango de '+str(limite_km)+'Kilometros'
        generarErrorDelSistema(respuesta_str)
        return respuesta,respuesta_str

    #-Buscamos a los choferes mas cercanos usando la ecuacion de haversine, esta ecuacion obtiene la distancia en
    #kilometros en base a dos coordenadas(lat,long), sin embargo el resultado de la ecuacion retorna los kilometros
    #en LINEA RECTA, sin tomar en cuenta calles, rutas u otros factores. Esto se hace así para evitar calculos de mas
    #que afecten el flujo y alenten el servidor
    #-El calculo se hace con las coordenandas del punto de origen (no necesariamente la posicion del cliente) VS las coordenanas del chofer
    lista_distancias = []
    for lista in lista_ubicacion_choferes:
        distancia = haversine(args['origen_lat'],args['origen_lng'],lista.latitud,lista.longitud)
        partial = {'distancia':distancia}
        lista_distancias.append(partial)

    for lista in lista_distancias:
        if lista['distancia'] <= limite_km:
            #si la distancia entre las coordenadas está en el rango del limite permitido entonces se procede a obtener
            #la informacion de los choferes cercanos con ayuda de las listas ya creadas
            for datos_chofer in lista_choferes_por_pais:
                partial = {

                }

            #si la distancia entre las coordenadas está en el rango del limite permitido entonces se procede a obtener la
            #información exacta de los kilometros a recorrer, el tiempo estimado, esto se hace con ayuda de google api matrix
            origen  = str(args['origen_lat'])+','+str(args['origen_lng'])
            destino = str(args['destino_lat'])+','+str(args['destino_lng'])
            informacion_viaje = googleMatrixAPI(origen,destino)


    return respuesta
# -----------------------------------------------------------------------------------------------------------------------
def findPais(coordenadas):
    '''Funcion encargada de buscar el pais en base a las coordenadas (lat,long) del cliente, esto con el motivo de hacer
    un primer filtrado de choferes, por ejemplo:
    - Si las coordenadas del cliente son provenientes de Mexico, se buscarán a los choferes que se hayan registrado con país
    de México
    - Si las coordenadas del cliente son provenientes de Costa Rica, se buscarán a los choferes que se hayan registrado con
    país de Costa Rica
    Existe un segundo filtro llamado findChoferesMasCercanos
    '''
    # - parseo a string las coordenadas
    coordenadas = str(coordenadas)
    # - uso de libreria de geolocalizacion
    geolocator = Nominatim(user_agent="rut")
    location = geolocator.reverse(coordenadas)
    #- se obtiene el diccionario con los datos del pais que corresponden a las coordenadas
    geolocalizador = location.raw
    findChoferesMasCercanos('','','')
    return geolocalizador['address']['country']


# -----------------------------------------------------------------------------------------------------------------------
def uploadDocumentosAuto(id_chofer, id_auto):
    files = {}
    if 'licencia' not in request.files and 'tarjeta' not in request.files and 'poliza' not in request.files and \
            'verificacion' not in request.files:
        return files

    files['licencia']     = ''
    files['tarjeta']      = ''
    files['poliza']       = ''
    files['verificacion'] = ''

    if 'licencia' in request.files:
        file_licencia = request.files['licencia']
        filename_licencia = secure_filename(file_licencia.filename)
        filename_licencia = filename_licencia.split('.')
        extension = filename_licencia[1]

        destino = app.config['CHOFERES_LICENCIA_PATH'] + str(id_chofer) + '/' + str(id_auto)
        if not os.path.exists(destino):
            os.makedirs(destino, 0o755)
        # -- se renombra la imagen con formato: <id_auto>_<fecha>.<extension> --#
        filename_licencia = str(id_auto) + '_' + str(datetime.now().microsecond) + '.' + extension
        # -se guarda la imagen
        file_licencia.save(os.path.join(destino, filename_licencia))
        files['licencia'] = filename_licencia

    if 'tarjeta' in request.files:
        file_tarjeta = request.files['tarjeta']
        filename_tarjeta = secure_filename(file_tarjeta.filename)
        filename_tarjeta = filename_tarjeta.split('.')
        extension = filename_tarjeta[1]

        destino = app.config['CHOFERES_TARJETA_PATH'] + str(id_chofer) + '/' + str(id_auto)
        if not os.path.exists(destino):
            os.makedirs(destino, 0o755)
        # -- se renombra la imagen con formato: <id_auto>_<fecha>.<extension> --#
        filename_tarjeta = str(id_auto) + '_' + str(datetime.now().microsecond) + '.' + extension
        # -se guarda la imagen
        file_tarjeta.save(os.path.join(destino, filename_tarjeta))
        files['tarjeta'] = filename_tarjeta

    if 'poliza' in request.files:
        file_poliza = request.files['poliza']
        filename_poliza = secure_filename(file_poliza.filename)
        filename_poliza = filename_poliza.split('.')
        extension = filename_poliza[1]

        destino = app.config['CHOFERES_POLIZA_PATH'] + str(id_chofer) + '/' + str(id_auto)
        if not os.path.exists(destino):
            os.makedirs(destino, 0o755)
        # -- se renombra la imagen con formato: <id_auto>_<fecha>.<extension> --#
        filename_poliza = str(id_auto) + '_' + str(datetime.now().microsecond) + '.' + extension
        # -se guarda la imagen
        file_poliza.save(os.path.join(destino, filename_poliza))
        files['poliza'] = filename_poliza

    if 'verificacion' in request.files:
        file_verificacion = request.files['verificacion']
        filename_verificacion = secure_filename(file_verificacion.filename)
        filename_verificacion = filename_verificacion.split('.')
        extension = filename_verificacion[1]

        destino = app.config['CHOFERES_POLIZA_PATH'] + str(id_chofer) + '/' + str(id_auto)
        if not os.path.exists(destino):
            os.makedirs(destino, 0o755)
        # -- se renombra la imagen con formato: <id_auto>_<fecha>.<extension> --#
        filename_verificacion = str(id_auto) + '_' + str(datetime.now().microsecond) + '.' + extension
        # -se guarda la imagen
        file_verificacion.save(os.path.join(destino, filename_verificacion))
        files['verificacion'] = filename_verificacion

    return files
# -----------------------------------------------------------------------------------------------------------------------



# -----------------------------------------------------------------------------------------------------------------------


# -----------------------------------------------------------------------------------------------------------------------