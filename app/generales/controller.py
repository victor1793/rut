from flask import request, session
from flask_restful import Resource, reqparse
from app.helpers.helper import create_success_response, computeMD5hash
from app.helpers.decorator import authenticate_request
from app import logger, app
from app.generales.service import getListaAutos, getFAQS, getEstados,generarErrorDelSistema,createNotificacionFCM
from app.helpers.helper import create_error_response
from app.excepciones.exception import BusinessException
import json

class ListaAutosRestService(Resource):
    '''peticion de autenticacion'''
    @authenticate_request
    def get(self,firebase_token,tipo):
        autos_json = ''
        try:
            lista = getListaAutos()
            autos_json = {'Autos':lista}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(autos_json)

class PreguntasFrecuentes(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('tipo', required=True, help='parametro tipo es requerido')
        info_faqs = parser.parse_args()
        faqs_json = ''
        try:
            lista = getFAQS(info_faqs['tipo'])
            faqs_json = {'FAQS':lista}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(faqs_json)

class Estados(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('pais', required=True, help='parametro pais es requerido')
        info = parser.parse_args()
        estados_json = ''
        try:
            lista = getEstados(info['pais'])
            estados_json = {'Estados':lista}
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(estados_json)


class NotificacionesRestService(Resource):
    @authenticate_request
    def post(self,firebase_token,tipo):
        '''servicio para generar notificaciones para chofer y cliente'''
        notificacion_json = ''
        try:

            i=createNotificacionFCM(firebase_token,tipo)
        except BusinessException as e:
            return create_error_response(e.message)
        return create_success_response(notificacion_json)