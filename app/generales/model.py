from app import db

class FAQS(db.Model):

    __tablename__ = 'preguntas_frecuentes'
    id            = db.Column(db.Integer, primary_key=True)
    pregunta_es   = db.Column(db.String(500))
    pregunta_eng  = db.Column(db.String(500))
    respuesta_es  = db.Column(db.String(500))
    respuesta_eng = db.Column(db.String(500))
    tipo          = db.Column(db.Integer)
    fecha         = db.Column(db.DateTime)
    fecha_baja    = db.Column(db.DateTime)
    status        = db.Column(db.Integer)

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def dump(self):
        return dict([ ('id',self.id), ('pregunta_es',self.pregunta_es), ('pregunta_eng',self.pregunta_eng) , ('respuesta_es',self.respuesta_es),
            ('respuesta_eng',self.respuesta_eng), ('fecha',self.fecha),('fecha_baja',self.fecha_baja) ])

class ErroresEnSistema(db.Model):

    __tablename__ = 'errores_en_sistema'
    id     = db.Column(db.Integer, primary_key=True)
    error  = db.Column(db.Text)
    fecha  = db.Column(db.DateTime)
    status = db.Column(db.Integer)

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def dump(self):
        return dict([ ('id',self.id), ('error',self.error), ('fecha',self.fecha) ])
